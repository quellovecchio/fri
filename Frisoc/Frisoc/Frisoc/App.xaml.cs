using Frisoc.Pages;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using System.Threading.Tasks;
using Plugin.Media;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace Frisoc
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();
            MainPage = new NavigationPage(new LoginPage());
        }

        protected override void OnStart()
        {
            Console.WriteLine("OnStart Called");
        }

        protected override void OnSleep()
        {
            Console.WriteLine("OnSleep Called");
        }

        protected override void OnResume()
        {
            Console.WriteLine("OnResume Called");
        }

    }
}
