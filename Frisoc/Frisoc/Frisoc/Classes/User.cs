using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
using Frisoc;
using Frisoc.Classes.AWS;

namespace Frisoc.Classes
{
    public class User
    {
        private long id;
        public long Id { get => id; set => id = value; }

        private string username;
        public string Username
        {
            get { return username; }
            set { username = value; }
        }

        private Image profilePicture;
        public Image ProfilePicture
        {
            get { return profilePicture; }
            set { profilePicture = value; }
        }

        private Image coverPicture;
        public Image CoverPicture
        {
            get { return coverPicture; }
            set { coverPicture = value; }
        }

        private string bio;
        public string Bio
        {
            get { return bio; }
            set { bio = value; }
        }

        private int followersNumber;
        public int FollowersNumber
        {
            get { return followersNumber; }
            set { followersNumber = value; }
        }

        private int followedNumber;
        public int FollowedNumber
        {
            get { return followedNumber; }
            set { followedNumber = value; }
        }

        private int postNumber;
        public int PostNumber
        {
            get { return postNumber; }
            set { postNumber = value; }
        }

        private List<User> usersFollowedList;
        public List<User> UsersFollowedList
        {
            get { return usersFollowedList; }
            set { usersFollowedList = value; }
        }

        private List<User> usersFollowersList;
        public List<User> UsersFollowersList
        {
            get { return usersFollowersList; }
            set { usersFollowersList = value; }
        }
        
        private List<Post> posts;
        public List<Post> Posts
        {
            get { return posts; }
            set { posts = value; }
        }

        public List<Visitation> Visitations { get => visitations; set => visitations = value; }

        private List<Visitation> visitations;


        // Standard constructor: puts everything to bullshit
        public User()
        {
            this.Username = "Pippo";
            this.CoverPicture = new Image { Source = "laboratorio.jpg" };
            this.ProfilePicture = new Image { Source = "Albert_Einstein.jpg" };
            this.Bio = "Hipster Coffee Drinker";
            this.FollowersNumber = 300;
            this.FollowedNumber = 90;
            this.PostNumber = 33;
            this.UsersFollowersList = new List<User>();
            this.UsersFollowedList = new List<User>();
            this.Posts = new List<Post>();
            this.Visitations = new List<Visitation>();
            this.Id = 0;
        }
        public User(string username, long id)
        {
            this.Username = username;
            this.CoverPicture = new Image { Source = "laboratorio.jpg" };
            this.ProfilePicture = AWSS3Service.GetProfilePicture(username);
            this.Bio = "Hipster Coffee Drinker";
            this.FollowersNumber = 0;
            this.FollowedNumber = 0;
            this.PostNumber = 0;
            this.UsersFollowersList = new List<User>();
            this.UsersFollowedList = new List<User>();
            this.Posts = new List<Post>();
            this.Id = id;
        }

        public override bool Equals(object obj)
        {
            User user2 = (User)obj;
            // Comparing usernames because they are unique
            return user2.Username == this.Username;
        }

        public override int GetHashCode()
        {
            return Convert.ToInt32(Id);
        }
    }
}
