﻿using Plugin.Media;
using Plugin.Media.Abstractions;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Frisoc.Classes.Media
{
    public static class MediaController
    {

        /// <summary>
        /// Takes a photo and returns the filepath of the photo
        /// </summary>
        /// <returns>
        /// Returns the filepath if the operation is succeded,
        /// 0 if the operation was interrupted, 1 if the camera is not supported
        /// </returns>

        public static async Task<string> TakeAPhotoAsync(int maxWidthHeight)
        {
            await CrossMedia.Current.Initialize();

            if (!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakePhotoSupported)
            {
                return "1";
            }

            Console.WriteLine("permission passed");

            var file = await CrossMedia.Current.TakePhotoAsync(new Plugin.Media.Abstractions.StoreCameraMediaOptions
            {
                SaveToAlbum = true,
                MaxWidthHeight = maxWidthHeight,
                PhotoSize = PhotoSize.MaxWidthHeight,
                DefaultCamera = CameraDevice.Rear,
                Directory = "ProfilePictures",
                Name = DateTime.Now.ToString("yyyy.MM.dd.mm.ss"),
                CompressionQuality = 50
            });

            if (file == null)
                return "0";

            else
            {
                Console.WriteLine("photo taken");
                Console.WriteLine("the photo path is " + file.Path);
                return file.Path;
            }
        }

        /// <summary>
        /// Lets the user pick a photo from the gallery and returns the filepath of the photo
        /// </summary>
        /// <returns>
        /// Returns the filepath if the operation is succeded,
        /// 0 if the operation was interrupted, 2 if gallery pick is not supported
        /// </returns>
        public static async Task<string> ChoosePhotoFromGalleryAsync()
        {
            await CrossMedia.Current.Initialize();
            if (!CrossMedia.Current.IsPickPhotoSupported)
                return "2";

            MediaFile file = await CrossMedia.Current.PickPhotoAsync();

            if (file == null)
                return "0";
            else
                return file.Path;
        }
    }
}
