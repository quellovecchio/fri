﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Frisoc.Classes
{
    public class Formatting
    {
        public enum FormattingType
        {
            Normal,
            Bold,
            Italic,
            Underlined
        }

        public static string GetFormatting(FormattingType formatting)
        {
            // enum.ToString("g") returns the name of the enum
            return formatting.ToString("g");
        }

        // Gets a string value and returns enum
        public static FormattingType SetFormatting(string formatting)
        {
            if (formatting == "normal" || formatting == "Normal")
                return FormattingType.Normal;
            if (formatting == "bold" || formatting == "Bold")
                return FormattingType.Bold;
            if (formatting == "italic" || formatting == "Italic")
                return FormattingType.Italic;
            if (formatting == "underlined" || formatting == "Underlined")
                return FormattingType.Underlined;
            else
                throw new InvalidOperationException("FormattingType string value invalid for purpose. " +
                                                    "Check if it was (normal, Normal, bold, Bold, italic, Italic, underlined, Underlined");
        }
    }
}
