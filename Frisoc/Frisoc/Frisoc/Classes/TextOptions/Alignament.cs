﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Frisoc.Classes
{
    public class Alignament
    {
        public enum AlignamentType
        {
            Left,
            Center,
            Right
        }

        public static string GetAlignament(AlignamentType alignament)
        {
            // enum.ToString("g") returns the name of the enum
            return alignament.ToString("g");
        }

        // Gets a string value and returns enum
        public static AlignamentType SetAlignament(string alignament)
        {
            if (alignament == "left" || alignament == "Left")
                return AlignamentType.Left;
            if (alignament == "center" || alignament == "Center")
                return AlignamentType.Center;
            if (alignament == "right" || alignament == "Right")
                return AlignamentType.Center;
            else
                throw new InvalidOperationException("AlignamentType string value invalid for purpose. " +
                                                    "Check if it was (left, Left, center, Center, right or Right");
        }
    } 
}
