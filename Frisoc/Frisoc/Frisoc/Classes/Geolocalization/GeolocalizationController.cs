﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace Frisoc.Classes.Geolocalization
{
    public static class GeolocalizationController
    {
        public static async Task<string> GetAddressFromLocationAsync(Location l)
        {
            double lat = l.Latitude;
            double lon = l.Longitude;

            var placemarks = await Geocoding.GetPlacemarksAsync(lat, lon);

            var placemark = placemarks?.FirstOrDefault();
            if (placemark != null)
            {
                Console.WriteLine($"Feature Name: {placemark.FeatureName}, SubLocality Name: {placemark.SubLocality}, SubThoroughfare Name: {placemark.SubThoroughfare}, SubAdminArea Name: {placemark.SubAdminArea}, Locality Name: {placemark.Locality}");
                if (placemark.FeatureName == "" || placemark.FeatureName == null || placemark.FeatureName == "Unnamed Road" || placemark.FeatureName.Contains('0')
                    || placemark.FeatureName.Contains('1') || placemark.FeatureName.Contains('2') || placemark.FeatureName.Contains('3') || placemark.FeatureName.Contains('4')
                    || placemark.FeatureName.Contains('5') || placemark.FeatureName.Contains('6') || placemark.FeatureName.Contains('7') || placemark.FeatureName.Contains('8')
                    || placemark.FeatureName.Contains('9'))
                {
                    if (placemark.Locality == "" || placemark.FeatureName == null)
                        return $"{placemark.CountryName}";
                    else
                        return $"{placemark.Locality}, {placemark.CountryName}";
                }
                else
                    return $"{placemark.FeatureName}, {placemark.CountryName}";
            }
            else
                return "Unknown Location";
        }

        public static async Task<Location> GetLocationFromAddressAsync(String address)
        {
            try
            {
                var locations = await Geocoding.GetLocationsAsync(address);

                var location = locations?.FirstOrDefault();
                if (location != null)
                    return new Location(location.Latitude, location.Longitude);
                else
                    return null;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }


        public static async Task<string> GetCountryFromLocationAsync(Location l)
        {
            double lat = l.Latitude;
            double lon = l.Longitude;

            var placemarks = await Geocoding.GetPlacemarksAsync(lat, lon);

            var placemark = placemarks?.FirstOrDefault();
            if (placemark != null)
            {
                Console.WriteLine($"Feature Name: {placemark.FeatureName}, SubLocality Name: {placemark.SubLocality}, SubThoroughfare Name: {placemark.SubThoroughfare}, SubAdminArea Name: {placemark.SubAdminArea}, Locality Name: {placemark.Locality}");
                return placemark.CountryName;
            }
            else
                return "Unknown Location";
        }

        public static async Task<Location> GetActualLocation()
        {
            try
            {
                var request = new GeolocationRequest(GeolocationAccuracy.Medium);
                var location = await Geolocation.GetLocationAsync(request);
                return location;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
