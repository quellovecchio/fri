﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Frisoc.Classes.Constants
{
    static class Strings
    {
        public static String HtmlFormattedUsageConditions = "<html><head><meta charset='utf-8'><meta name='viewport' content='width=device-width'><title>Privacy Policy</title><style> body { font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; padding:1em; } </style></head><body><h2><strong>Terms and Conditions</strong></h2><p>Welcome to Fri!</p><p>These terms and conditions outline the rules and regulations for the use of Fri s.r.l.'s Website, located at www.fri.com.</p><p>By accessing this website we assume you accept these terms and conditions.Do not continue to use Fri if you do not agree to take all of the terms and conditions stated on this page.</p><p>The following terminology applies to these Terms and Conditions, Privacy Statement and Disclaimer Notice and all Agreements: 'Client', 'You' and 'Your' refers to you, the person log on this website and compliant to the Company’s terms and conditions. 'The Company', 'Ourselves', 'We', 'Our' and 'Us', refers to our Company. 'Party', 'Parties', or 'Us', refers to both the Client and ourselves.All terms refer to the offer, acceptance and consideration of payment necessary to undertake the process of our assistance to the Client in the most appropriate manner for the express purpose of meeting the Client’s needs in respect of provision of the Company’s stated services, in accordance with and subject to, prevailing law of Netherlands. Any use of the above terminology or other words in the singular, plural, capitalization and/or he/she or they, are taken as interchangeable and therefore as referring to same.</p><h3><strong>Cookies</strong></h3><p>We employ the use of cookies. By accessing Fri, you agreed to use cookies in agreement with the Fri s.r.l.'s Privacy Policy.</p><p>Most interactive websites use cookies to let us retrieve the user’s details for each visit. Cookies are used by our website to enable the functionality of certain areas to make it easier for people visiting our website. Some of our affiliate/advertising partners may also use cookies.</p><h3><strong>License</strong></h3><p>Unless otherwise stated, Fri s.r.l.and/or its licensors own the intellectual property rights for all material on Fri. All intellectual property rights are reserved. You may access this from Fri for your own personal use subjected to restrictions set in these terms and conditions.</p><p>You must not:</p><ul><li>Republish material from Fri</li><li>Sell, rent or sub-license material from Fri</li><li>Reproduce, duplicate or copy material from Fri</li><li>Redistribute content from Fri</li></ul><p>This Agreement shall begin on the date hereof.</p><p>Parts of this website offer an opportunity for users to post and exchange opinions and information in certain areas of the website.Fri s.r.l.does not filter, edit, publish or review Comments prior to their presence on the website.Comments do not reflect the views and opinions of Fri s.r.l., its agents and/or affiliates. Comments reflect the views and opinions of the person who post their views and opinions.To the extent permitted by applicable laws, Fri s.r.l.shall not be liable for the Comments or for any liability, damages or expenses caused and/or suffered as a result of any use of and/or posting of and/or appearance of the Comments on this website.</p><p>Fri s.r.l.reserves the right to monitor all Comments and to remove any Comments which can be considered inappropriate, offensive or causes breach of these Terms and Conditions.</p><p>You warrant and represent that:</p><ul><li>You are entitled to post the Comments on our website and have all necessary licenses and consents to do so;</li><li>The Comments do not invade any intellectual property right, including without limitation copyright, patent or trademark of any third party;</li><li>The Comments do not contain any defamatory, libelous, offensive, indecent or otherwise unlawful material which is an invasion of privacy</li><li>The Comments will not be used to solicit or promote business or custom or present commercial activities or unlawful activity.</li></ul><p>You hereby grant Fri s.r.l.a non-exclusive license to use, reproduce, edit and authorize others to use, reproduce and edit any of your Comments in any and all forms, formats or media.</p><h3><strong>Hyperlinking to our Content</strong></h3><p>The following organizations may link to our Website without prior written approval:</p><ul><li>Government agencies;</li><li>Search engines;</li><li>News organizations;</li><li>Online directory distributors may link to our Website in the same manner as they hyperlink to the Websites of other listed businesses; and</li><li>System wide Accredited Businesses except soliciting non-profit organizations, charity shopping malls, and charity fundraising groups which may not hyperlink to our Web site.</li></ul><p>These organizations may link to our home page, to publications or to other Website information so long as the link: (a) is not in any way deceptive; (b) does not falsely imply sponsorship, endorsement or approval of the linking party and its products and/or services; and(c) fits within the context of the linking party’s site.</p><p>We may consider and approve other link requests from the following types of organizations:</p><ul><li>commonly-known consumer and/or business information sources;</li><li>dot.com community sites;</li><li>associations or other groups representing charities;</li><li>online directory distributors;</li><li>internet portals;</li><li>accounting, law and consulting firms; and</li><li>educational institutions and trade associations.</li></ul><p>We will approve link requests from these organizations if we decide that: (a) the link would not make us look unfavorably to ourselves or to our accredited businesses; (b) the organization does not have any negative records with us; (c) the benefit to us from the visibility of the hyperlink compensates the absence of Fri s.r.l.; and(d) the link is in the context of general resource information.</p><p>These organizations may link to our home page so long as the link: (a) is not in any way deceptive; (b) does not falsely imply sponsorship, endorsement or approval of the linking party and its products or services; and(c) fits within the context of the linking party’s site.</p><p>If you are one of the organizations listed in paragraph 2 above and are interested in linking to our website, you must inform us by sending an e-mail to Fri s.r.l.. Please include your name, your organization name, contact information as well as the URL of your site, a list of any URLs from which you intend to link to our Website, and a list of the URLs on our site to which you would like to link. Wait 2-3 weeks for a response.</p><p>Approved organizations may hyperlink to our Website as follows:</p><ul><li>By use of our corporate name; or</li><li>By use of the uniform resource locator being linked to; or</li><li>By use of any other description of our Website being linked to that makes sense within the context and format of content on the linking party’s site.</li></ul><p>No use of Fri s.r.l.'s logo or other artwork will be allowed for linking absent a trademark license agreement.</p><h3><strong>iFrames</strong></h3><p>Without prior approval and written permission, you may not create frames around our Webpages that alter in any way the visual presentation or appearance of our Website.</p><h3><strong>Content Liability</strong></h3><p>We shall not be hold responsible for any content that appears on your Website.You agree to protect and defend us against all claims that is rising on your Website. No link(s) should appear on any Website that may be interpreted as libelous, obscene or criminal, or which infringes, otherwise violates, or advocates the infringement or other violation of, any third party rights.</p><h3><strong>Your Privacy</strong></h3><p>Please read Privacy Policy</p><h3><strong>Reservation of Rights</strong></h3><p>We reserve the right to request that you remove all links or any particular link to our Website. You approve to immediately remove all links to our Website upon request. We also reserve the right to amen these terms and conditions and it’s linking policy at any time. By continuously linking to our Website, you agree to be bound to and follow these linking terms and conditions.</p><h3><strong>Removal of links from our website</strong></h3><p>If you find any link on our Website that is offensive for any reason, you are free to contact and inform us any moment. We will consider requests to remove links but we are not obligated to or so or to respond to you directly.</p><p>We do not ensure that the information on this website is correct, we do not warrant its completeness or accuracy; nor do we promise to ensure that the website remains available or that the material on the website is kept up to date.</p><h3><strong>Disclaimer</strong></h3><p>To the maximum extent permitted by applicable law, we exclude all representations, warranties and conditions relating to our website and the use of this website.Nothing in this disclaimer will:</p><ul><li>limit or exclude our or your liability for death or personal injury;</li><li>limit or exclude our or your liability for fraud or fraudulent misrepresentation;</li><li>limit any of our or your liabilities in any way that is not permitted under applicable law; or</li><li>exclude any of our or your liabilities that may not be excluded under applicable law.</li></ul><p>The limitations and prohibitions of liability set in this Section and elsewhere in this disclaimer: (a) are subject to the preceding paragraph; and(b) govern all liabilities arising under the disclaimer, including liabilities arising in contract, in tort and for breach of statutory duty.</p><p>As long as the website and the information and services on the website are provided free of charge, we will not be liable for any loss or damage of any nature.</p></body></html>";
        public static String HtmlFormattedPrivacyPolicy = @"<!DOCTYPE html><html><head><meta charset='utf-8'><meta name='viewport' content='width=device-width'><title>Privacy Policy</title><style> body { font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; padding:1em; } </style></head>
      <body>
      <h2> Privacy Policy</h2> <p>
                      Fri s.r.l.built the Fri app as
                      a Commercial app.This SERVICE is provided by
                      Fri s.r.l.and is intended for
                      use as is.
                    </p> <p>
                      This page is used to inform visitors regarding
                      our policies with the collection, use, and
                      disclosure of Personal Information if anyone decided to use
                      our Service.
                    </p> <p>
                      If you choose to use our Service, then you agree
                      to the collection and use of information in relation to this
                      policy.The Personal Information that we collect is
                      used for providing and improving the Service.
                      We will not use or share your
                      information with anyone except as described in this Privacy
                      Policy.
                    </p> <p>
                      The terms used in this Privacy Policy have the same meanings
                      as in our Terms and Conditions, which is accessible at
                      Fri unless otherwise defined in this Privacy
                      Policy.
                    </p> <p><strong>Information Collection and Use</strong></p> <p>
                      For a better experience, while using our Service,
                      we may require you to provide us with certain
                      personally identifiable information, including but not limited to position, interactions with other users.The
                      information that we request will be
                      retained by us and used as described in this privacy policy.
                    </p> <p>
                      The app does use third party services that may collect
                      information used to identify you.
                    </p> <div><p>
                        Link to privacy policy of third party service providers
                        used by the app
                      </p> <ul><li>Google Play Services</li><!----><!----><!----><!----><!----><!----><!----><!----><!----><!----></ul></div> <p><strong>Log Data</strong></p> <p>
                      We want to inform you that whenever
                      you use our Service, in a case of an error in the
                      app we collect data and information (through third
                      party products) on your phone called Log Data.This Log Data
                      may include information such as your device Internet
                      Protocol (“IP”) address, device name, operating system
                      version, the configuration of the app when utilizing
                      our Service, the time and date of your use of the
                      Service, and other statistics.
                    </p> <p><strong>Cookies</strong></p> <p>
                      Cookies are files with a small amount of data that are
                      commonly used as anonymous unique identifiers.These are
                      sent to your browser from the websites that you visit and
                      are stored on your device's internal memory.
                    </p> <p>
                      This Service does not use these “cookies” explicitly.
                      However, the app may use third party code and libraries that
                      use “cookies” to collect information and improve their
                      services.You have the option to either accept or refuse
                     these cookies and know when a cookie is being sent to your
                      device.If you choose to refuse our cookies, you may not be
                      able to use some portions of this Service.
                    </p> <p><strong>Service Providers</strong></p> <p>
                      We may employ third-party companies
                      and individuals due to the following reasons:
                    </p> <ul><li>To facilitate our Service;</li> <li>To provide the Service on our behalf;</li> <li>To perform Service-related services; or</li> <li>To assist us in analyzing how our Service is used.</li></ul> <p>
                      We want to inform users of this
                      Service that these third parties have access to your
                      Personal Information.The reason is to perform the tasks
                      assigned to them on our behalf. However, they are obligated
                      not to disclose or use the information for any other
                      purpose.
                    </p> <p><strong>Security</strong></p> <p>
                      We value your trust in providing us
                      your Personal Information, thus we are striving to use
                      commercially acceptable means of protecting it. But remember
                      that no method of transmission over the internet, or method
                      of electronic storage is 100% secure and reliable, and
                      we cannot guarantee its absolute security.
                    </p> <p><strong>Links to Other Sites</strong></p> <p>
                      This Service may contain links to other sites. If you click
                      on a third-party link, you will be directed to that site.
                      Note that these external sites are not operated by
                      us. Therefore, we strongly advise you to
                      review the Privacy Policy of these websites.
                      We have no control over and assume no
                      responsibility for the content, privacy policies, or
                      practices of any third-party sites or services.
                    </p> <p><strong>Children’s Privacy</strong></p> <p>
                      These Services do not address anyone under the age of 13.
                      We do not knowingly collect personally
                      identifiable information from children under 13. In the case
                      we discover that a child under 13 has provided
                      us with personal information,
                      we immediately delete this from our servers.If you
                      are a parent or guardian and you are aware that your child
                      has provided us with personal information, please contact
                      us so that we will be able to do
                      necessary actions.
                    </p> <p><strong>Changes to This Privacy Policy</strong></p> <p>
                      We may update our Privacy Policy from
                      time to time. Thus, you are advised to review this page
                      periodically for any changes. We will
                      notify you of any changes by posting the new Privacy Policy on this page.These changes are effective immediately after they are posted on this page.</p> <p><strong>Contact Us</strong></p> <p>If you have any questions or suggestions about our Privacy Policy, do not hesitate to contact us at fri @info.com.</p> <p>      ";
    }
}
