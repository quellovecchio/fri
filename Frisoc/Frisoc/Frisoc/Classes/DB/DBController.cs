﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace Frisoc.Classes.DB
{
    /// <summary>
    /// Class which will handle the work with the db
    /// </summary>
    public class DBController
    {
        static readonly string connectionString = "Server=192.168.43.41; Port=5432; User Id=postgres; Password=postgres; Database=Fri;";

        NpgsqlConnection connection;

        public DBController()
        {
            connection = new NpgsqlConnection(connectionString);
        }

        internal List<Post> FetchFeedPagePosts(long userId)
        {
            try
            {
                connection.Open();
                NpgsqlCommand command = connection.CreateCommand();
                command.CommandText = "SELECT * " +
                                      "FROM ( SELECT posts.id, coordinatesx, coordinatesy, publicationdate, user_id, body, users.id, username FROM posts JOIN users ON posts.user_id=users.id WHERE posts.publicationdate IN ( SELECT publicationdate FROM posts AS T2 WHERE T2.id = id ORDER BY publicationdate DESC LIMIT 15 ) ) AS foo " +
                                      "WHERE user_id = 8 OR user_id IN ( SELECT user2_id FROM user_follows_user WHERE user1_id = " + userId.ToString() + " ) ";
                List<Post> postList = new List<Post>();
                NpgsqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    Console.WriteLine("1: " + reader[1].ToString() + "2: " + reader[2].ToString() + "3: " + reader[3].ToString() + "4: " + reader[4].ToString() + "5: " + reader[5].ToString() + "6: " + reader[6].ToString() + "7: " + reader[7].ToString());
                    User u = new User(reader[7].ToString(), (long)reader[6]);
                    Post p = new Post((long)reader[0], (decimal)reader[2], (decimal)reader[1], u, reader[5].ToString(), CreateDateFromDbInfo(reader[3].ToString()));
                    postList.Add(p);
                }
                connection.Close();


                return postList;
            }
            catch (Exception ex)
            {
                connection.Close();
                MessagingCenter.Send(this, "DbError");
                Console.WriteLine(ex.Message);
                return new List<Post>();
            }
        }

        private DateTime CreateDateFromDbInfo(string date)
        {
            String dateWithoutTime = date.Remove(10);
            Console.WriteLine(dateWithoutTime.Substring(6, 4) + " " + dateWithoutTime.Substring(3, 2) + " " + dateWithoutTime.Substring(0, 2));
            DateTime d = new DateTime(int.Parse(dateWithoutTime.Substring(6, 4)), int.Parse(dateWithoutTime.Substring(3, 2)), int.Parse(dateWithoutTime.Substring(0, 2)));
            return d;
        }

        internal List<Visitation> FetchFeedPageVisitations(long userId)
        {
            try
            {
                connection.Open();
                NpgsqlCommand command = connection.CreateCommand();
                command.CommandText = "SELECT * " +
                                      "FROM ( SELECT * FROM visitations JOIN users ON visitations.user_id=users.id WHERE visitations.publicationdate IN ( SELECT publicationdate FROM visitations AS T2 WHERE T2.id = id ORDER BY publicationdate DESC LIMIT 15 ) ) AS foo " +
                                      "WHERE user_id = 8 OR user_id IN ( SELECT user2_id FROM user_follows_user WHERE user1_id = " + userId.ToString() + " ) ";
                List<Visitation> visitationList = new List<Visitation>();
                NpgsqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    Console.WriteLine("1: " + reader[1].ToString() + "2: " + reader[2].ToString() + "3: " + reader[3].ToString() + "4: " + reader[4].ToString() + "5: " + reader[5].ToString() + "6: " + reader[6].ToString());
                    User u = new User(reader[6].ToString(), (long)reader[4]);
                    Visitation v = new Visitation((long)reader[0], u, (decimal)reader[1], (decimal)reader[2], CreateDateFromDbInfo(reader[3].ToString()));
                    visitationList.Add(v);
                }
                connection.Close();

                return visitationList;
            }
            catch (Exception ex)
            {
                connection.Close();
                MessagingCenter.Send(this, "DbError");
                Console.WriteLine(ex.Message);
                return new List<Visitation>();
            }
        }

        internal bool User1FollowsUser2(User currentUser, User u)
        {
            try
            {
                connection.Open();
                NpgsqlCommand command = connection.CreateCommand();
                command.CommandText = "SELECT * FROM user_follows_user WHERE user1_id = " + currentUser.Id + " AND user2_id = " + u.Id;
                List<Visitation> visitationList = new List<Visitation>();
                NpgsqlDataReader reader = command.ExecuteReader();
                reader.Read();
                bool r = reader.HasRows;
                connection.Close();
                return r;
            }
            catch (Exception ex)
            {
                connection.Close();
                Console.WriteLine(ex.Message);
                return false;
            }
        }

        internal List<User> FindUsersByUsernameAndVisitations(string username, Location l)
        {
            try
            {
                List<User> users = new List<User>();

                NpgsqlConnection connection = new NpgsqlConnection(connectionString);
                connection.Open();
                NpgsqlCommand command = connection.CreateCommand();
                command.CommandText = "SELECT * " +
                                      "FROM users " +
                                      "WHERE username = '" + username + "'";
                NpgsqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    User u = new User(reader[1].ToString(), (long)reader[0]);
                    users.Add(u);
                }
                connection.Close();

                connection.Open();
                command = connection.CreateCommand();
                double coordinatesx = l.Longitude;
                double coordinatesy = l.Latitude;
                double valueA = coordinatesx - 0.1D;
                double valueB = coordinatesx + 0.1D;
                double valueC = coordinatesy - 0.1D;
                double valueD = coordinatesy + 0.1D;
                command.CommandText = "SELECT * " +
                                      "FROM visitations JOIN users ON visitations.user_id=users.id " +
                                      "WHERE coordinatesx BETWEEN " + valueA.ToString().Replace(',', '.') + " AND " + valueB.ToString().Replace(',', '.') +
                                      " AND coordinatesy BETWEEN " + valueC.ToString().Replace(',', '.') + " AND " + valueD.ToString().Replace(',', '.') +
                                      " ORDER BY publicationdate ASC ";
                Dictionary<User, int> usersByNumberOfVisitations = new Dictionary<User, int>();
                reader = command.ExecuteReader();

                while (reader.Read())
                {
                    User u = new User(reader[6].ToString(), (long)reader[4]);
                    if (usersByNumberOfVisitations.ContainsKey(u))
                    {
                        int c = 0;
                        usersByNumberOfVisitations.TryGetValue(u, out c);
                        usersByNumberOfVisitations.Remove(u);
                        c++;
                        usersByNumberOfVisitations.Add(u, c);
                    }
                    else
                        usersByNumberOfVisitations.Add(u, 1);
                }
                connection.Close();

                users.AddRange(usersByNumberOfVisitations.Keys);

                return users;
            }
            catch (Exception ex)
            {
                connection.Close();
                Console.WriteLine(ex.Message);
                return new List<User>();
            }
        }

        internal List<Post> FindPostsInTheNearings(Location l)
        {
            try
            {
                double coordinatesx = l.Longitude;
                double coordinatesy = l.Latitude;
                double valueA = coordinatesx - 0.1D;
                double valueB = coordinatesx + 0.1D;
                double valueC = coordinatesy - 0.1D;
                double valueD = coordinatesy + 0.1D;
                connection.Open();
                NpgsqlCommand command = connection.CreateCommand();
                command.CommandText = "SELECT * " +
                                      "FROM posts JOIN users ON posts.user_id=users.id " +
                                      "WHERE coordinatesx BETWEEN " + valueA.ToString().Replace(',', '.') + " AND " + valueB.ToString().Replace(',', '.') +
                                      " AND coordinatesy BETWEEN " + valueC.ToString().Replace(',', '.') + " AND " + valueD.ToString().Replace(',', '.') +
                                      " ORDER BY publicationdate ASC ";
                List<Post> postList = new List<Post>();
                NpgsqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    Console.WriteLine("1: " + reader[1].ToString() + "2: " + reader[2].ToString() + "3: " + reader[3].ToString() + "4: " + reader[4].ToString() + "5: " + reader[5].ToString() + "6: " + reader[6].ToString() + "7: " + reader[7].ToString());
                    User u = new User(reader[7].ToString(), (long)reader[6]);
                    Post p = new Post((long)reader[0], (decimal)reader[2], (decimal)reader[1], u, reader[5].ToString(), CreateDateFromDbInfo(reader[3].ToString()));
                    postList.Add(p);
                }
                connection.Close();

                return postList;
            }
            catch (Exception ex)
            {
                connection.Close();
                MessagingCenter.Send(this, "DbError");
                Console.WriteLine(ex.Message);
                return new List<Post>();
            }
        }

        internal Task InsertNewVisitation(long userId, Location currentLocation)
        {
            return Task.Run(() =>
            {
                Boolean flag = false;
                long id = 0;
                while (flag == false)
                {
                    // Generates the unique id
                    id = GenerateRandomLongId();

                    // Checks if the id is really unique performing a query and checking if the result is empty
                    try
                    {
                        connection.Open();
                        NpgsqlCommand command = connection.CreateCommand();
                        command.CommandText = "SELECT * " +
                                              "FROM visitations" +
                                              "WHERE id = '" + id.ToString() + "'";
                        NpgsqlDataReader reader = command.ExecuteReader();

                        if (!reader.HasRows)
                            flag = true;
                        else
                            flag = false;

                        connection.Close();
                    }
                    catch (Exception ex)
                    {
                        connection.Close();
                        Console.WriteLine(ex.Message);
                        MessagingCenter.Send(this, "DbError");
                        break;
                    }
                }
                try
                {
                    // Then puts the visitation into the DB
                    connection.Open();
                    NpgsqlCommand secondCommand = connection.CreateCommand();
                    Console.WriteLine("longitude = " + currentLocation.Longitude.ToString() + ", latitude = " + currentLocation.Latitude.ToString());
                    String query = "INSERT INTO visitations (id, coordinatesx, coordinatesy, publicationdate, user_id) VALUES ('" + id + "', " + currentLocation.Longitude.ToString().Replace(',', '.') + ", " + currentLocation.Latitude.ToString().Replace(',', '.') + ", '" + DateTime.Now.ToString("yyyy-MM-dd") + "', '" + userId + "')";
                    Console.WriteLine(query);
                    secondCommand.CommandText = query;
                    secondCommand.ExecuteNonQuery();
                    connection.Close();
                }
                catch (Exception ex)
                {
                    connection.Close();
                    Console.WriteLine(ex.Message);
                    MessagingCenter.Send(this, "DbError");
                }
            });
        }

        internal int CountPostsByUser(long id)
        {
            try
            {
                connection.Open();
                NpgsqlCommand command = connection.CreateCommand();
                command.CommandText = "SELECT COUNT(*) " +
                                      "FROM posts " +
                                      "WHERE user_id = " + id;
                NpgsqlDataReader reader = command.ExecuteReader();
                reader.Read();
                int r = Convert.ToInt32(reader[0]);
                connection.Close();
                return r;
            }
            catch (Exception ex)
            {
                connection.Close();
                Console.WriteLine(ex.Message);
                return 0;
            }
        }

        internal long RetrieveUserIdFromUsername(String username)
        {
            try
            {
                connection.Open();
                NpgsqlCommand command = connection.CreateCommand();
                command.CommandText = "SELECT id " +
                                      "FROM users " +
                                      "WHERE username = '" + username + "'";
                NpgsqlDataReader reader = command.ExecuteReader();

                reader.Read();
                long r = (long)reader[0];
                connection.Close();
                return r;
            }
            catch (Exception ex)
            {
                connection.Close();
                Console.WriteLine(ex.Message);
                return 0;
            }
        }

        internal Task InsertNewPost(long userId, Location currentLocation, String body)
        {
            return Task.Run(() =>
            {
                Boolean flag = false;
                long id = 0;
                while (flag == false)
                {
                    // Generates the unique id
                    id = GenerateRandomLongId();

                    // Checks if the id is really unique performing a query and checking if the result is empty
                    try
                    {
                        connection.Open();
                        NpgsqlCommand command = connection.CreateCommand();
                        command.CommandText = "SELECT * " +
                                              "FROM posts " +
                                              "WHERE id = '" + id.ToString() + "'";
                        NpgsqlDataReader reader = command.ExecuteReader();

                        if (!reader.HasRows)
                            flag = true;
                        else
                            flag = false;

                        connection.Close();
                    }
                    catch (Exception ex)
                    {
                        connection.Close();
                        Console.WriteLine(ex.Message);
                        MessagingCenter.Send(this, "DbError");
                        break;
                    }
                }
                try
                {
                    // Then puts the visitation into the DB
                    connection.Open();
                    NpgsqlCommand secondCommand = connection.CreateCommand();
                    Console.WriteLine("longitude = " + currentLocation.Longitude.ToString() + ", latitude = " + currentLocation.Latitude.ToString());
                    String query = "INSERT INTO posts (id, coordinatesx, coordinatesy, publicationdate, user_id, body) VALUES ('" + id + "', " + currentLocation.Longitude.ToString().Replace(',', '.') + ", " + currentLocation.Latitude.ToString().Replace(',', '.') + ", '" + DateTime.Now.ToString("yyyy-MM-dd") + "', '" + userId + "', '" + body + "')";
                    Console.WriteLine(query);
                    secondCommand.CommandText = query;
                    secondCommand.ExecuteReader();
                    MessagingCenter.Send(this, "IdPostValue", id);
                    connection.Close();
                }
                catch (Exception ex)
                {
                    connection.Close();
                    Console.WriteLine(ex.Message);
                    MessagingCenter.Send(this, "DbError");
                }
            });
        }

        private static long GenerateRandomLongId()
        {
            Random rand1 = new Random();
            Random rand2 = new Random();
            long t = rand1.Next() * rand2.Next();
            long r = Math.Abs(t);
            Console.WriteLine("The generated id is: " + r.ToString());
            return r;
        }

        internal List<Post> FetchUserPagePosts(long userId)
        {
            try
            {
                connection.Open();
                NpgsqlCommand command = connection.CreateCommand();
                command.CommandText = "SELECT * " +
                                      "FROM posts JOIN users ON posts.user_id=users.id " +
                                      "WHERE users.id=" + userId;
                List<Post> postList = new List<Post>();
                NpgsqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    Console.WriteLine("1: " + reader[1].ToString() + "2: " + reader[2].ToString() + "3: " + reader[3].ToString() + "4: " + reader[4].ToString() + "5: " + reader[5].ToString() + "6: " + reader[6].ToString() + "7: " + reader[7].ToString());
                    User u = new User(reader[7].ToString(), (long)reader[6]);
                    Post p = new Post((long)reader[0], (decimal)reader[2], (decimal)reader[1], u, reader[5].ToString(), CreateDateFromDbInfo(reader[3].ToString()));
                    postList.Add(p);
                }

                connection.Close();

                return postList;
            }
            catch (Exception ex)
            {
                connection.Close();
                MessagingCenter.Send(this, "DbError");
                Console.WriteLine(ex.Message);
                return new List<Post>();
            }
        }

        internal List<Visitation> FetchUserPageVisitations(long userId)
        {
            try
            {
                connection.Open();
                NpgsqlCommand command = connection.CreateCommand();
                command.CommandText = "SELECT * " +
                                      "FROM visitations JOIN users ON visitations.user_id=users.id " +
                                      "WHERE  users.id=" + userId;
                List<Visitation> visitationList = new List<Visitation>();
                NpgsqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    Console.WriteLine("1: " + reader[1].ToString() + "2: " + reader[2].ToString() + "3: " + reader[3].ToString() + "4: " + reader[4].ToString() + "5: " + reader[5].ToString() + "6: " + reader[6].ToString());
                    User u = new User(reader[6].ToString(), (long)reader[4]);
                    Visitation v = new Visitation((long)reader[0], u, (decimal)reader[1], (decimal)reader[2], CreateDateFromDbInfo(reader[3].ToString()));
                    visitationList.Add(v);
                }

                connection.Close();

                return visitationList;
            }
            catch (Exception ex)
            {
                connection.Close();   
                MessagingCenter.Send(this, "DbError");
                Console.WriteLine(ex.Message);
                return new List<Visitation>();
            }
        }
        
        public int FetchUserFollowers(long userId)
        {
            try
            {
                connection.Open();
                NpgsqlCommand command = connection.CreateCommand();
                command.CommandText = "SELECT count(user2_id)" +
                                      "FROM user_follows_user " +
                                      "WHERE user2_id=" + userId;
                NpgsqlDataReader reader = command.ExecuteReader();

                reader.Read();
                int followers = Convert.ToInt32(reader[0]);
                connection.Close();
                return followers;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.InnerException.Message);
                return 0;
            }
        }


        internal int FetchUserFollowed(long userId)
        {

            try
            {
                connection.Open();
                NpgsqlCommand command = connection.CreateCommand();
                command.CommandText = "SELECT count(user1_id) " +
                                      "FROM user_follows_user " +
                                      "WHERE user1_id = " + userId;
                NpgsqlDataReader reader = command.ExecuteReader();
                
                reader.Read();
                int followed = Convert.ToInt32(reader[0]);
                connection.Close();
                return followed;
            }
            catch (Exception ex)
            {
                connection.Close();
                Console.WriteLine(ex.InnerException.Message);
                return 0;
            }
        }

        public void AddFollowerToUser(User followingUser, User followedUser)
        {
            try
            {
                connection.Open();
                NpgsqlCommand command = connection.CreateCommand();
                command.CommandText = "INSERT INTO user_follows_user(user1_id, user2_id) " +
                                      "VALUES(" + followingUser.Id.ToString() + "," + followedUser.Id.ToString() + ")";
                int i = command.ExecuteNonQuery();
                connection.Close();
            }
            catch (Exception ex)
            {
                connection.Close();
                Console.WriteLine(ex.InnerException.Message);
            }
        }
    }
}