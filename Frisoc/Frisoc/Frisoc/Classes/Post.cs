﻿using Frisoc.Classes.AWS;
using Frisoc.Pages;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace Frisoc.Classes
{
    public class Post : Visitation
    {
        /*private long id;
        public long Id { get => id; set => id = value; }*/

        // User who wrote the post
        /*private User writer;
        public User Writer
        {
            get { return writer; }
            set { writer = value; }
        }
        // Date and time of publication
        private DateTime publicationDate;
        public DateTime PublicationDate
        {
            get { return publicationDate; }
            set { publicationDate = value; }
        }*/

        // body of the post
        private string textBody;
        public string TextBody { get => textBody; set => textBody = value; }

        /*// location of the post
        private Location place;
        public Location Place { get => place; set => place = value; }*/

        // photo of the post
        private Image photo;

        public Image Photo { get => photo; set => photo = value; }
        

        // Standard constructor, puts everything to bullshit
        public Post(User writer)
        {
            /*Writer = new User();
            PublicationDate = DateTime.Now;*/
            TextBody = "Proprio così";
            Place = new Location(47.673988, -122.121513);
            Place.Altitude = 100;
            //Id = 0;
            Photo = new Image();
            Photo.Source = "Albert_Einstein.jpg";
        }

        public Post(long id, decimal coordinatesx, decimal coordinatesy, User writer, String body, DateTime publicationDate) : base(id, writer, coordinatesx, coordinatesy, publicationDate)
        {
            /*Writer = writer;
            PublicationDate = publicationdate;*/
            TextBody = body;
            Photo = new Image();
            Photo.Source = AWSS3Service.GetPostPicture(id).Source;
            Place = new Location((float)coordinatesx, (float)coordinatesy);
            //Id = id;
        }

        /*public override bool Equals(object obj)
        {
            return ((obj is Post) && ((Post)obj).Id == Id);
        }

        public override int GetHashCode()
        {
            return (int)Place.Latitude * 33 + (int)Place.Longitude * 33 + TextBody.Length * 33;
        }*/

        internal bool IsLoaded(List<Post> postsDisplayed)
        {
            foreach (Post p in postsDisplayed)
            {
                if (this.Id == p.Id)
                    return true;
            }
            return false;
        }
    }
}
