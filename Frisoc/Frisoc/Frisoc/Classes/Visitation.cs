﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
using Xamarin.Essentials;

namespace Frisoc.Classes
{
    public class Visitation
    {

        private long id;
        public long Id { get => id; set => id = value; }

        private User user;
        public User Writer { get => user; set => user = value; }


        private Location place;
        public Location Place { get => place; set => place = value; }

        private DateTime publicationDate;
        public DateTime PublicationDate { get => publicationDate; set => publicationDate = value; }


        public Visitation()
        {
            this.Writer = new User();
            this.Place = new Location(47.673988, -122.121513);
            this.Place.Altitude = 100;
            this.PublicationDate = DateTime.Now;
        }

        public Visitation(long id, User u, decimal coordinatesx, decimal coordinatesy, DateTime publicationDate)
        {
            this.Id = id;
            this.Writer = u;
            this.Place = new Location((float)coordinatesy, (float)coordinatesx);
            this.PublicationDate = publicationDate;
        }
        public override bool Equals(object obj)
        {
            return ((obj is Post) && ((Visitation)obj).Id == Id);
        }

        public override int GetHashCode()
        {
            return (int)Place.Latitude * 33 + (int)Place.Longitude * 33;
        }

        internal bool IsLoaded(List<Visitation> visitationsDisplayed)
        {
            foreach (Visitation v in visitationsDisplayed)
            {
                if (this.Id == v.Id)
                    return true;
            }
            return false;
        }
    }
}
