﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.IO;

using Amazon;
using Amazon.CognitoIdentity;
using Amazon.CognitoIdentityProvider;
using Amazon.CognitoIdentityProvider.Model;
using Amazon.Runtime;
using Amazon.Extensions.CognitoAuthentication;
using Amazon.S3;
using Amazon.S3.Transfer;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.DataModel;
using Amazon.DynamoDBv2.Model;

using Frisoc.Classes.AWS;
using Frisoc.Classes.DB;

namespace Frisoc.Classes
{
    // AWS Utility tool
    // Initialize it with username, email, password of your user
    public class AWSUtils
    {
        public static RegionEndpoint AWSRegion = RegionEndpoint.EUWest2;
        public static String ClientID = "2s87sprlpv5stcilp18fjaaadh";
        public static String UserpoolID = "eu-west-2_z7qsqTKQN";
        public static String IdentitypoolID = "eu-west-2:cf8c0270-826c-463a-872f-e719ef06afa3";
        public static String IdentityProvider = "cognito-idp.eu-west-2.amazonaws.com/";

        private AmazonCognitoIdentityProviderClient client;
        public AmazonCognitoIdentityProviderClient Client
        {
            get { return client; }
            set { client = value; }
        }

        private String email;
        public string Email
        {
            get { return email; }
            set { email = value; }
        }
        private String username;
        public string Username
        {
            get { return username; }
            set { username = value; }
        }
        private String password;
        public string Password
        {
            get { return password; }
            set { password = value; }
        }

        // Standard constructor
        // Put email, username, password of the user
        public AWSUtils(String email, String username, String password)
        {
            Client = new AmazonCognitoIdentityProviderClient(new AnonymousAWSCredentials(), AWSRegion);
            Email = email;
            Username = username;
            Password = password;
        }

        // Returns a User given the ID to the server
        internal static async Task<User> GetUserFromIdentification(String username)
        {
            CognitoAWSCredentials credentials = new CognitoAWSCredentials(
            IdentitypoolID,
            AWSRegion
            );
            User u = new User();
            // Code which resolves data from the DB

            // Code which resolves data from the webserver downloading the profile picture
            var filePath = Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), "Fri", "Images");
            var s3Client = new AmazonS3Client(credentials, AWSRegion);
            try
            {
                TransferUtilityDownloadRequest request = new TransferUtilityDownloadRequest()
                {
                    BucketName = "fri-profile-pictures",
                    Key = username + ".jpg",
                    FilePath = filePath
                };

                TransferUtility transferUtility = new TransferUtility(s3Client);

                System.Threading.CancellationToken cancellationToken = new System.Threading.CancellationToken();
                await transferUtility.DownloadAsync(request, cancellationToken);
                u.ProfilePicture.Source = Path.Combine(filePath, username, ".jpg");
            }
            catch (Exception e)
            {
                u.ProfilePicture = new Image() { Source = "default_user_propic.png" };
            }

            return u;
        }

        // Call this to sign up a user
        public async Task<bool> SignUpUser()
        {
            SignUpRequest sur = new SignUpRequest();

            try
            {
                sur.ClientId = ClientID;
                sur.Username = username;
                sur.Password = password;
                sur.UserAttributes.Add(
                    new AttributeType
                    {
                        Name = "email",
                        Value = email,
                    });
                await Client.SignUpAsync(sur);

                var client = new AmazonDynamoDBClient(new CognitoAWSCredentials(
                IdentitypoolID,
                AWSRegion), AWSRegion);

                string tableName = "Users";

                var request = new PutItemRequest
                {
                    TableName = tableName,
                    Item = new Dictionary<string, AttributeValue>()
                    {
                        { "username", new AttributeValue { S = Username }},
                        { "bio", new AttributeValue { S = "" }},
                    }
                };
                await client.PutItemAsync(request);

                return true;
            }
            catch (UsernameExistsException)
            {
                return false;
            }
            catch (ConditionalCheckFailedException)
            {
                return false;
            }
            catch (InternalServerErrorException)
            {
                return false;
            }
        }

        // Call this to sign in a user
        public async Task<bool> SignInUser()
        {
            // Amazon hocus pocus
            CognitoUser user = new CognitoUser(Username, ClientID, new CognitoUserPool(UserpoolID, ClientID, Client), Client);

            try
            {
                AuthFlowResponse response = await user.StartWithSrpAuthAsync(new InitiateSrpAuthRequest()
                {
                    Password = Password
                });
                if (response.AuthenticationResult != null)
                {
                    GetUserRequest getUserRequest = new GetUserRequest();
                    getUserRequest.AccessToken = response.AuthenticationResult.AccessToken;

                    var getTheUser = Client.GetUserAsync(getUserRequest);

                    var userAttributes = getTheUser.Result.UserAttributes;

                    user.Attributes = new Dictionary<string, string>();

                    if (userAttributes != null)
                    {
                        for (int i = 0; i < userAttributes.Count; ++i)
                        {
                            // Print out the attributes that are stored in Cognito User Pool for the user
                            user.Attributes.Add(userAttributes[i].Name, userAttributes[i].Value);
                            System.Diagnostics.Debug.WriteLine(string.Format("Name: {0} Value: {1}", userAttributes[i].Name, userAttributes[i].Value));
                        }
                    }
                    // Puts the "username" key into the properties. This is at the scope of verifying lately your access.
                    App.Current.Properties.Add("username", user.UserID);
                    return true;
                }
                else
                    return false;
            }
            catch (UserNotFoundException)
            {
                return false;
            }
            catch (Exception ex)
            {
                System.Console.WriteLine(ex.ToString());
                return false;
            }
        }

        public bool CreateActiveUserObject()
        {
            Console.WriteLine("Started creating the user to put into properties");
            User u = new User();
            Object usernameObject;
            try
            {
                DBController dBController = new DBController();
                Application.Current.Properties.TryGetValue("username", out usernameObject);
                u.Username = (string)usernameObject;
                Console.WriteLine("Name found: " + u.Username);
                Console.WriteLine("Started awaiting the picture");
                Image result = AWSS3Service.GetProfilePicture(u.Username);
                u.ProfilePicture = result;
                Console.WriteLine("Picture resolved");
                u.Id = dBController.RetrieveUserIdFromUsername(u.Username);
                Application.Current.Properties.Add("user", u);
                Console.WriteLine("User value added to the properties");
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine("User value not added to the properties");
                Console.WriteLine(e.Message);
                return false;
            }
        }

        public async Task<bool> PutPropicIntoDatabaseAsync(string nickname, string filepath)
        {
            if (await AWSS3Service.PutProfilePictureAsync(nickname, filepath))
                return true;
            else
                return false;
        }

        public async Task<bool> PutPostPictureIntoDatabaseAsync(long postId, string filepath)
        {
            if (await AWSS3Service.PutPostPictureAsync(postId, filepath))
                return true;
            else
                return false;
        }
    }
}
