﻿using Amazon;
using Amazon.Runtime;
using Amazon.S3;
using Amazon.S3.Model;
using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
using System.IO;
using System.Threading;
using Amazon.S3.Transfer;

namespace Frisoc.Classes.AWS
{
    public static class AWSS3Service
    {

        private const string propicBucketName = "fri-profile-pictures";
        private const string postBucketName = "fri-post-pictures";
        private const string amazonUri = "https://s3.eu-west-2.amazonaws.com/";
        private const string accessId = "AKIA2IXTLFIAUJCIFV7Y";
        private const string secretKey = "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";
        private static readonly RegionEndpoint bucketRegion = RegionEndpoint.EUWest2;
        private static AmazonS3Client client = GetClient();

        internal static Image GetProfilePicture(String nickname)
        {

            Image propic;
            String resourceName = nickname + ".jpg";

            try
            {
                Console.WriteLine("Trying to access the resource " + resourceName);
                // Waits the ReadObjectDataAsync method
                propic = ReadImageDataAsync(propicBucketName, resourceName);
                Console.WriteLine("The access to the resource " + resourceName + " has been grant");
                return propic;
            }
            catch (Exception e)
            {
                Console.WriteLine("Could not download the resource " + resourceName);
                Console.WriteLine("Exception message: " + e.Message);
                return null;
            }
        }

        // "postCode" to be given with Post.code
        internal static Image GetPostPicture(long postCode)
        {

            Image propic;
            String resourceName = postCode + ".jpg";

            try
            {
                Console.WriteLine("Trying to access the resource " + resourceName);
                // Waits the ReadObjectDataAsync method
                propic = ReadImageDataAsync(postBucketName, resourceName);
                Console.WriteLine("The access to the resource " + resourceName + " has been grant");
                return propic;
            }
            catch (Exception e)
            {
                Console.WriteLine("Could not download the resource " + resourceName);
                Console.WriteLine("Exception message: " + e.Message);
                return null;
            }
        }

        internal static async Task<bool> PutProfilePictureAsync(String username, String filePath)
        {
            try
            {
                var uploadRequest = new TransferUtilityUploadRequest
                {
                    FilePath = filePath,
                    BucketName = propicBucketName,
                    Key = username + ".jpg",
                    CannedACL = S3CannedACL.PublicRead
                };

                TransferUtility transferUtility = new TransferUtility(client);
                
                await transferUtility.UploadAsync(uploadRequest);

                return true;
            }

            catch (AmazonS3Exception e)
            {
                Console.WriteLine(e.Message, e.InnerException);
                return false;
            }
        }

        internal static async Task<bool> PutPostPictureAsync(long postCode, string filepath)
        {
            try
            {
                // Create a PutObject request
                PutObjectRequest request = new PutObjectRequest
                {
                    BucketName = postBucketName,
                    Key = postCode.ToString() + ".jpg",
                    FilePath = filepath,
                    CannedACL = S3CannedACL.PublicRead
                };

                // Put object
                PutObjectResponse response = await client.PutObjectAsync(request);
                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return false;
            }
        }

        internal static async Task<bool> DeleteProfilePictureAsync(String username)
        {
            // Not tested code
            try
            {
                // Create a DeleteObject request
                DeleteObjectRequest request = new DeleteObjectRequest
                {
                   BucketName = propicBucketName,
                   Key = username + ".jpg"
                 };
    
                // Issue request
                await client.DeleteObjectAsync(request);
                return true;
            }

            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return false;
            }
        }

        private static Image ReadImageDataAsync(String bucketName, String resourceName)
        {
            Image result = new Image
            {
                Source = new UriImageSource
                {
                   Uri = new Uri(amazonUri + bucketName + "/" + resourceName)
                }
            };
            return result;
        }

        public static Uri RetrievePostImageUri(long id)
        {
            return new Uri(amazonUri + postBucketName + "/" + id.ToString());
        }

        public static Uri RetrieveUserImageUri(String username)
        {
            return new Uri(amazonUri + postBucketName + "/" + username.ToString());
        }

        private static AmazonS3Client GetClient()
        {
            Console.WriteLine("Started creation of S3 client");
            AmazonS3Client client = new AmazonS3Client(new BasicAWSCredentials(accessId, secretKey), bucketRegion);
            Console.WriteLine("S3 client created with success");
            return client;
        }

        private static void OnUploadProgressEvent(object sender, UploadProgressArgs e)
        {
            Console.WriteLine("Uploading the picture " + sender.ToString() + "... " + e.TransferredBytes.ToString());
        }
    }
}
