﻿using System;
using System.Collections.Generic;
using System.Text;
using static Frisoc.Classes.Alignament;
using static Frisoc.Classes.Formatting;

namespace Frisoc.Classes
{
    public class Paragraph
    {
        // Body of the paragraph. Contains text
        private string body;
        public string Body
        {
            get { return body; }
            set { body = value; }
        }

        // Formatting option of the text. Can be "normal", "bold", "italic" or "underlined"
        private FormattingType formattingMethod;
        public FormattingType FormattingMethod
        {
            get { return formattingMethod; }
            set { formattingMethod = value; }
        }

        // Alignament of the text. Can be "left", "center" or "right"
        private AlignamentType alignamentMethod;
        public AlignamentType AlignamentMethod
        {
            get { return alignamentMethod; }
            set { alignamentMethod = value; }
        }

        // Boolean that tells if the paragraph is a picture.
        private bool isPicture;
        public bool IsPicture
        {
            get { return isPicture; }
            set { isPicture = value; }
        }

        // Progressive number for the picture (if there is more pictures, used to retrieve from the server)
        /*pictureNumber;
        public int PictureNumber
        {
            get { return pictureNumber; }
            set { pictureNumber = value; }
        }*/

        // Constructor in case the paragraph is a picture
        public Paragraph(string pictureNumber)
        {
            IsPicture = true;
            //PictureNumber = pictureNumber;
        }

        // Construtor in case the paragraph is a text
        public Paragraph(string body, string formatting, string alignament)
        {
            IsPicture = false;
            Body = body;
            FormattingMethod = SetFormatting(formatting);
            AlignamentMethod = SetAlignament(alignament);
        }
    }
}
