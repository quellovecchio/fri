﻿using Frisoc.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using Frisoc.Customs;
using Xamarin.Forms.Maps;
using Frisoc.Classes.Geolocalization;
using Xamarin.Essentials;
using Frisoc.Classes.DB;
using Frisoc.Classes.Media;

namespace Frisoc.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class NewPostPage : ContentPage
    {
        private Location currentLocation;
        private Boolean isLocationSet;

        public NewPostPage()
        {
            InitializeComponent();
            Icon = "new_icon.png";
            currentLocation = new Location();
            isLocationSet = false;
        }

        protected override void OnAppearing()
        {
            ReleaseContentAsync();
            InitPageAsync();
        }

        private async void InitPageAsync()
        {
            MakePostButton.IsEnabled = false;
            MakeVisitationButton.IsEnabled = false;
            InformationLayout.Children.Clear();
            map.Pins.Clear();
            InformationLayout.Children.Add(new Label
            {
                Text = "Waiting for your position...",
                FontSize = 16,
                HorizontalOptions = LayoutOptions.Center
            });
            InformationLayout.Children.Add(new ActivityIndicator
            {
                Color = Color.Gray,
                IsRunning = true,
                HorizontalOptions = LayoutOptions.Center,
                VerticalOptions = LayoutOptions.Center
            });
            currentLocation = await GeolocalizationController.GetActualLocation();
            isLocationSet = true;
            if (currentLocation == null)
            {
                await DisplayAlert("Error", "Could not load your position. Are you sure geolocalization is on?", "Cancel");
                InformationLayout.Children.Clear();
                InformationLayout.Children.Add(new Label
                {
                    Text = "Unknown place",
                    FontSize = 16,
                    HorizontalOptions = LayoutOptions.Center
                });
            }
            else
            {
                map.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(currentLocation.Latitude, currentLocation.Longitude), Distance.FromMiles(.1)));
                InformationLayout.Children.Clear();
                InformationLayout.Children.Add(new Label
                {
                    Text = await GeolocalizationController.GetAddressFromLocationAsync(currentLocation),
                    FontSize = 16,
                    HorizontalOptions = LayoutOptions.Center
                });
                MakePostButton.IsEnabled = true;
                MakeVisitationButton.IsEnabled = true;
                var position = new Position(currentLocation.Latitude, currentLocation.Longitude);
                var pin = new Pin
                {
                    Type = PinType.SearchResult,
                    Position = position,
                    Label = "You",
                };
                map.Pins.Add(pin);
            }
        }

        void MakeVisitationButton_Clicked(object sender, EventArgs e)
        {
            if (isLocationSet)
            {
                Console.WriteLine("Started intent of making a visitation");
                LockAndDisplayWaitAnimation();
                SendVisitationToDB();
                ReleaseContentAsync();
                DisplayAlert("Success", "You visited this place!", "OK");
            }
            else
            {
                DisplayAlert("Error", "Your actual position could not be found", "OK");
            }
        }

        private async void SendVisitationToDB()
        {
            User currentUser = (User)Application.Current.Properties["user"];
            MessagingCenter.Subscribe<DBController>(this, "DbError", async (sender) =>
            {
                await DisplayAlert("Uh-Oh", "There has been a problem with the DB", "Cancel");
            });
            DBController dbController = new DBController();
            await dbController.InsertNewVisitation(currentUser.Id, currentLocation);
            MessagingCenter.Unsubscribe<DBController>(this, "DbError");
        }

        async void MakePostButton_Clicked(object sender, EventArgs e)
        {
            if (isLocationSet)
            {
                Console.WriteLine("Started intent of making a post");
                await Navigation.PushAsync(new AddPostPage(currentLocation));
                ReleaseContentAsync();
            }
            else
            {
                await DisplayAlert("Error", "Your actual position could not be found", "OK");
            }
        }


        void RefreshButton_Clicked(object sender, EventArgs e)
        {
            InitPageAsync();
        }

        public void LockAndDisplayWaitAnimation()
        {
            LoadingAnimationView.IsVisible = true;
            TransparentBackground.IsVisible = true;
            LoadingAnimationView.FadeTo(1, 300);
            TransparentBackground.FadeTo(1, 300);
        }
        internal async void ReleaseContentAsync()
        {
            TransparentBackground.IsVisible = false;
            LoadingAnimationView.IsVisible = false;
            await LoadingAnimationView.FadeTo(0, 400);
        }
    }
}