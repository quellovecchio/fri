﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Frisoc.Classes;
using Frisoc.Customs;
using Frisoc.Pages;
using Frisoc.Classes.AWS;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using Amazon.Extensions.CognitoAuthentication;
using Frisoc.Classes.DB;
using Xamarin.Essentials;
using Frisoc.Classes.Geolocalization;

namespace Frisoc.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class FeedPage : ContentPage
    {
        private List<Visitation> visitationsToDisplay;
        private List<Post> postsToDisplay;
        private List<Visitation> visitationsDisplayed;
        private List<Post> postsDisplayed;
        private DBController dbController;
        private Boolean isUiOutdate;

        public List<Visitation> VisitationsToDisplay { get => visitationsToDisplay; set => visitationsToDisplay = value; }
        public List<Post> PostsToDisplay { get => postsToDisplay; set => postsToDisplay = value; }
        public DBController DbController { get => dbController; set => dbController = value; }
        public List<Visitation> VisitationsDisplayed { get => visitationsDisplayed; set => visitationsDisplayed = value; }
        public List<Post> PostsDisplayed { get => postsDisplayed; set => postsDisplayed = value; }
        public bool IsUiOutdate { get => isUiOutdate; set => isUiOutdate = value; }

        // Standard constructor, mainly for testing purposes
        public FeedPage()
        {
            IsUiOutdate = false;
            PostsDisplayed = new List<Post>();
            VisitationsDisplayed = new List<Visitation>();
            InitializeComponent();
            AddSearchViewToPage();
            Icon = "feed_icon.png";
            DbController = new DBController();
        }

        protected override void OnAppearing()
        {
            ReleaseContent();
            InitPage();
        }

        private async void InitPage()
        {
            FeedStackLayout.RotateTo(180);
            FetchDataFromServerAsync();
            Task.Run(() => FeedLoader());
            AnimatePosts();
        }

        private void AddSearchViewToPage()
        {
            SearchView searchView = new SearchView(this);
            HeaderLayout.Children.Add(searchView);
        }

        private async Task FetchDataFromServerAsync()
        {
            MessagingCenter.Subscribe<DBController>(this, "DbError", async (sender) =>
            {
                await DisplayAlert("Uh-Oh", "There has been a problem with the DB", "Cancel");
            });
            User currentUser = (User)Application.Current.Properties["user"];
            PostsToDisplay =  await Task.FromResult(DbController.FetchFeedPagePosts(currentUser.Id));
            VisitationsToDisplay = await Task.FromResult(DbController.FetchFeedPageVisitations(currentUser.Id));
            MessagingCenter.Unsubscribe<DBController>(this, "DbError");
        }
        

        // Constructor in case we have a list of posts to display.
        // The boolean controls the loading of the user
        /*public FeedPage(List<Post> posts)
        {
            InitializeComponent();
            FeedLoader(posts);
        }*/

        private async void FeedLoader()
        {
            List<Visitation> orderedElementsToBeVisualized = new List<Visitation>();

            foreach (Post p in PostsToDisplay)
            {
                if (!p.IsLoaded(PostsDisplayed))
                {
                    orderedElementsToBeVisualized.Add((Post)p);
                }
            }
            PostsToDisplay.Clear();
            foreach (Visitation v in VisitationsToDisplay)
            {
                if (!v.IsLoaded(VisitationsDisplayed))
                {
                    orderedElementsToBeVisualized.Add((Visitation)v);
                }
            }
            VisitationsToDisplay.Clear();
            orderedElementsToBeVisualized.Sort((x, y) => -DateTime.Compare(y.PublicationDate, x.PublicationDate));
            if (orderedElementsToBeVisualized.Count > 0)
            {
                IsUiOutdate = true;
                //FeedStackLayout.Opacity = 0;
                await LoadElements(orderedElementsToBeVisualized);
            }
        }

        private async Task LoadElements(List<Visitation> orderedElementsToBeVisualized)
        {
            foreach (Object o in orderedElementsToBeVisualized)
            {
                if (o is Post)
                {
                    await LoadPost((Post)o);
                    PostsDisplayed.Add((Post)o);
                    IsUiOutdate = true;
                }
                else
                {
                    await LoadVisitation((Visitation)o);
                    VisitationsDisplayed.Add((Visitation)o);
                    IsUiOutdate = true;
                }
            }
        }

        private Task LoadVisitation(Visitation v)
        {
            return Task.Run(() =>
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    VisitationView vv = new VisitationView(v);
                    FeedStackLayout.Children.Add(vv);
                });
            });
        }

        // Function which displays a post.
        public Task LoadPost(Post p)
        {
            return Task.Run(() =>
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    PostView pv = new PostView(p);
                    FeedStackLayout.Children.Add(pv);
                });
            });
        }

        // method which animates the posts into the poststodisplay list
        private void AnimatePosts()
        {
            if (IsUiOutdate == true)
            {
                FeedStackLayout.Opacity = 1;
                IsUiOutdate = false;
            }
        }

        public async void LockAndDisplayWaitAnimation()
        {
            TransparentBackground.IsVisible = true;
            LoadingAnimationView.IsVisible = true;
            await TransparentBackground.FadeTo(1, 400);
            await LoadingAnimationView.FadeTo(1, 400);
        }

        public void ReleaseContent()
        {
            TransparentBackground.IsVisible = false;
            LoadingAnimationView.IsVisible = false;
            TransparentBackground.Opacity = 0;
            LoadingAnimationView.Opacity = 0;
        }
    }
}