﻿using Frisoc.Classes;
using Frisoc.Classes.AWS;
using Frisoc.Classes.DB;
using Frisoc.Customs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;


namespace Frisoc.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class UserPage : ContentPage
    {
        private User user;
        private List<Visitation> visitationsToDisplay;
        private List<Post> postsToDisplay;
        private List<Visitation> visitationsDisplayed;
        private List<Post> postsDisplayed;
        private DBController dbController;
        private Boolean isUiOutdate;

        public User User { get => user; set => user = value; }
        public List<Visitation> VisitationsToDisplay { get => visitationsToDisplay; set => visitationsToDisplay = value; }
        public List<Post> PostsToDisplay { get => postsToDisplay; set => postsToDisplay = value; }
        public DBController DbController { get => dbController; set => dbController = value; }
        public List<Visitation> VisitationsDisplayed { get => visitationsDisplayed; set => visitationsDisplayed = value; }
        public List<Post> PostsDisplayed { get => postsDisplayed; set => postsDisplayed = value; }
        public bool IsUiOutdate { get => isUiOutdate; set => isUiOutdate = value; }


        public UserPage(User user)
        {
            InitializeComponent();
            PostStackLayout.RotateTo(180);
            DbController = new DBController();
            Icon = "user_icon.png";
            User = user;
            NameLabel.Text = User.Username;
            ProfileImage.Source = User.ProfilePicture.Source;
            PostNumber.Text = User.PostNumber.ToString();
            SetFollowersAndFollowed(user.Id);
            SetPostCount(user.Id);
            Title = user.Username;
            IsUiOutdate = false;
            PostsDisplayed = new List<Post>();
            VisitationsDisplayed = new List<Visitation>();
            SetFollowAndSettingsButtonVisibility(User);
        }

        /* Costruttore per test*/
        public UserPage()
        {
            InitializeComponent();
            Icon = "user_icon.png";
            User = new User();
            NameLabel.Text = User.Username;
            ProfileImage.Source = User.ProfilePicture.Source;
            PostNumber.Text = User.PostNumber.ToString();
            FollowedNumber.Text = User.FollowedNumber.ToString();
            FollowersNumber.Text = User.FollowersNumber.ToString();
            Title = User.Username;
            PostsToDisplay = this.User.Posts;
            VisitationsToDisplay = User.Visitations;
            VisitationsToDisplay = this.User.Visitations;
            Loader();
            SetFollowAndSettingsButtonVisibility(User);
        }

        protected override void OnAppearing()
        {
            InitPage();
        }

        private async void InitPage()
        {
            PostStackLayout.RotateTo(180);
            FetchDataFromServerAsync();
            Task.Run(() => Loader());
            AnimatePosts();
        }

        private async Task FetchDataFromServerAsync()
        {
            MessagingCenter.Subscribe<DBController>(this, "DbError", async (sender) =>
            {
                await DisplayAlert("Uh-Oh", "There has been a problem with the DB", "Cancel");
            });
            PostsToDisplay = await Task.FromResult(DbController.FetchUserPagePosts(User.Id));
            VisitationsToDisplay = await Task.FromResult(DbController.FetchUserPageVisitations(User.Id));
            MessagingCenter.Unsubscribe<DBController>(this, "DbError");
        }

        private void SetFollowAndSettingsButtonVisibility(User u)
        {
            User currentUser = (User)Application.Current.Properties["user"];
            if (currentUser.Equals(u))
            {
                SettingsButton.IsVisible = true;
                FollowButton.IsVisible = false;
            }
            else
            {
                SettingsButton.IsVisible = false;
                if (!DbController.User1FollowsUser2(currentUser, u))
                    FollowButton.IsVisible = true;
                else
                    FollowButton.IsVisible = false;
            }

        }

        private void FollowButton_Clicked(object sender, EventArgs e)
        {
            User currentUser = (User)Application.Current.Properties["user"];
            // manage the database logic
            DbController.AddFollowerToUser(currentUser, User);
            // user of the user page
            User.FollowersNumber++;
            User.UsersFollowersList.Add(currentUser);
            int c = Int32.Parse(FollowersNumber.Text);
            c++;
            FollowersNumber.Text = c.ToString();
            // current user
            currentUser.FollowedNumber++;
            currentUser.UsersFollowedList.Add(User);
            Application.Current.Properties["user"] = currentUser;
            // graphics refresh
            FollowButton.IsVisible = false;
        }

        private async void NewPostButton_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new NewPostPage());
        }

        private async void SettingsButton_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new SettingsPage(this));
        }

        private async Task<string> DescriptionBuildAsync(User u)
        {
            
            Dictionary<Location, int> locations = GetLocations(u);
            /* TEST */
            Location l1 = new Location(47.673988, -82.121513);
            Location l2 = new Location(45.465018, 9.183372);
            Location l3 = new Location(47.673988, -122.121513);
            locations.Add(l1, 2);
            locations.Add(l2, 1);
            locations.Add(l3, 3);
            /*locations.OrderByDescending(key => key.Value);*/
            List<KeyValuePair<Location, int>> myList = locations.ToList();
            myList.Sort(
                delegate (KeyValuePair<Location, int> pair1,
                KeyValuePair<Location, int> pair2)
                {
                    return -(pair1.Value.CompareTo(pair2.Value));
                }
            );
            StringBuilder sb = new StringBuilder("");
            int numberOfLocations; //numero di Location da stampare (se sono meno di 3 o piu)
            if (locations.Count > 2)
                numberOfLocations = 3;
            else
                numberOfLocations = locations.Count;
            
            for (int i = 0; i < numberOfLocations; i++)
            {
                Location l = myList[i].Key;
                var placemarks = await Geocoding.GetPlacemarksAsync(l.Latitude, l.Longitude);
                var placemark = placemarks?.FirstOrDefault();
                if (placemark != null)
                {
                    sb.Append(
                        $"{placemark.CountryName}: " +
                        $"{myList[i].Value} volte\n");
                }
                else
                    sb.Append("Unknown");
            }
            return sb.ToString();
        }

        internal void reloadUserProfilePicture()
        {
            this.ProfileImage.Source = User.ProfilePicture.Source;
        }

        private Dictionary<Location, int> GetLocations (User u)
        {
            Dictionary<Location, int> locations = new Dictionary<Location, int>();
            foreach (Post p in u.Posts)
            {
                Location l = p.Place;
                if (locations.ContainsKey(l))
                    locations[l]++;
                else
                    locations.Add(l, 1);

            }
            foreach (Visitation v in u.Visitations)
            {
                Location l = v.Place;
                if (locations.ContainsKey(l))
                    locations[l]++;
                else
                    locations.Add(l, 1);

            }
            return locations;
        }

        private void AnimatePosts()
        {
            if (IsUiOutdate == true)
            {
                PostStackLayout.Opacity = 1;
                IsUiOutdate = false;
            }
        }

        public async Task SetDescriptionText(User u)
        {
            //this.Description.Text = await DescriptionBuildAsync(u);
        }

        private async void Loader()
        {
            List<Visitation> orderedElementsToBeVisualized = new List<Visitation>();

            foreach (Post p in PostsToDisplay)
            {
                if (!p.IsLoaded(PostsDisplayed))
                {
                    orderedElementsToBeVisualized.Add((Post)p);
                }
            }
            PostsToDisplay.Clear();
            foreach (Visitation v in VisitationsToDisplay)
            {
                if (!v.IsLoaded(VisitationsDisplayed))
                {
                    orderedElementsToBeVisualized.Add((Visitation)v);
                }
            }
            VisitationsToDisplay.Clear();
            orderedElementsToBeVisualized.Sort((x, y) => -DateTime.Compare(y.PublicationDate, x.PublicationDate));
            if (orderedElementsToBeVisualized.Count > 0)
            {
                IsUiOutdate = true;
                //FeedStackLayout.Opacity = 0;
                await LoadElements(orderedElementsToBeVisualized);
            }
        }

        private async Task LoadElements(List<Visitation> orderedElementsToBeVisualized)
        {
            foreach (Object o in orderedElementsToBeVisualized)
            {
                if (o is Post)
                {
                    await LoadPost((Post)o);
                    PostsDisplayed.Add((Post)o);
                    IsUiOutdate = true;
                }
                else
                {
                    await LoadVisitation((Visitation)o);
                    VisitationsDisplayed.Add((Visitation)o);
                    IsUiOutdate = true;
                }
            }
        }

        public Task LoadPost(Post p)
        {
            return Task.Run(() =>
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    PostView pv = new PostView(p);
                    pv.WriterProPic.IsVisible = false;
                    PostStackLayout.Children.Add(pv);
                });
            });
        }

        private Task LoadVisitation(Visitation v)
        {
            return Task.Run(() =>
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    VisitationView vv = new VisitationView(v);
                    vv.UserImage.IsVisible = false;
                    PostStackLayout.Children.Add(vv);
                });
            });
        }

        private void SetFollowersAndFollowed(long id)
        {
            int n = DbController.FetchUserFollowers(id);
            FollowersNumber.Text = n.ToString();
            FollowedNumber.Text = DbController.FetchUserFollowed(id).ToString();
            PostsToDisplay = DbController.FetchUserPagePosts(id);
            VisitationsToDisplay = DbController.FetchUserPageVisitations(id);
        }

        private void SetPostCount(long id)
        {
            int n = DbController.CountPostsByUser(id);
            PostNumber.Text = n.ToString();
        }

    }
}