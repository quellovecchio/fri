﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using Xamarin.Forms.Maps;
using Frisoc.Classes.Geolocalization;
using Xamarin.Essentials;
using Frisoc.Classes;
using Frisoc.Classes.DB;
using Frisoc.Classes.AWS;
using Frisoc.Classes.Media;
using Plugin.LocalNotifications;

namespace Frisoc.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AddPostPage : ContentPage
    {

        Location mapLocation;

        String photoPath;

        public AddPostPage(Location location)
        {
            InitializeComponent();
            pageLayout.IsVisible = false;
            mapLocation = location;
            photoPath = "";
            InitPage();
            pageLayout.IsVisible = true;
        }

        private async void InitPage()
        {
            photoPath = await MediaController.TakeAPhotoAsync(1024);
            Map.MoveToRegion(MapSpan.FromCenterAndRadius(new Position(mapLocation.Latitude, mapLocation.Longitude), Distance.FromMiles(.1)));
            PostImage.Source = photoPath;
            PostImage.GestureRecognizers.Add((new TapGestureRecognizer((view) => OnImageClicked())));
        }

        private async void OnImageClicked()
        {
            photoPath = await MediaController.TakeAPhotoAsync(1024);
            PostImage.Source = photoPath;
        }

        void ConfirmButton_Clicked(object sender, EventArgs e)
        {
            SendPostToDBAsync();
        }

        private async void SendPostToDBAsync()
        {
            try
            {
                // Checks the content of the editor and saves the string
                if (CheckTextBox())
                {
                    if ((bool)Application.Current.Properties["areNotificationsActivated"])
                        CrossLocalNotifications.Current.Show("Fri", "Your photo is being sent", 101);
                    String body = TextBox.Text;
                    long postId = 0;

                    // Uploads post informations to the server
                    User currentUser = (User)Application.Current.Properties["user"];
                    MessagingCenter.Subscribe<DBController>(this, "DbError", async (sender) =>
                    {
                        await DisplayAlert("Uh-Oh", "There has been a problem with the DB", "Cancel");
                    });
                    MessagingCenter.Subscribe<DBController, long>(this, "IdPostValue", async (sender, arg) =>
                    {
                        postId = arg;
                    });
                    DBController dbController = new DBController();
                    await dbController.InsertNewPost(currentUser.Id, mapLocation, body);
                    MessagingCenter.Unsubscribe<DBController>(this, "DbError");
                    MessagingCenter.Unsubscribe<DBController, long>(this, "IdPostValue");

                    // Uploads the photo to aws
                    await AWSS3Service.PutPostPictureAsync(postId, photoPath);

                    await DisplayAlert("Success!", "The post has been published", "OK");
                    CrossLocalNotifications.Current.Cancel(101);
                    await Navigation.PopAsync();
                }
                else
                {
                    InformationLabel.TextColor = Color.Red;
                    CrossLocalNotifications.Current.Cancel(101);
                    InformationLabel.Text = "The message is empty or too long";
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                await DisplayAlert("Uh-oh!", "There has been a problem with the network. Please retry", "OK");
                InformationLayout.Children.Clear();
                InitPage();
            }
        }

        private bool CheckTextBox()
        {
            String body = TextBox.Text;
            return (!(body == null) && !(body.Length > 99));
        }
    }
}