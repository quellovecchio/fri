﻿using Frisoc.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Amazon.CognitoIdentity;
using Amazon.CognitoIdentity.Model;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Amazon;
using Amazon.Extensions.CognitoAuthentication;
using Frisoc.Classes.AWS;
using System.IO;

namespace Frisoc.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LoginPage : ContentPage
    {
        public LoginPage()
        {
            InitializeComponent();
        }

        private void ClearNavigationStack()
        {
            int c = Navigation.NavigationStack.Count;
            for (int i = 0; i < c - 1; i++)
                Navigation.PopAsync();
        }

        void OnButtonClicked(object sender, EventArgs e)
        {
            if (CheckIfFieldsAreEmpty() == true)
            {
                LockAndDisplayWaitAnimation();
                InitializingOperations();
            }
        }

        private async void InitializingOperations()
        {
            AWSUtils aWSUtils = new AWSUtils("prova@prova.it", UsernameEntry.Text, PasswordEntry.Text);
            // Logs in User and checks his logging
            if (await aWSUtils.SignInUser())
            {
                //creates the user to put into properties
                if (aWSUtils.CreateActiveUserObject())
                {
                    Application.Current.Properties.Add("areNotificationsActivated", true);
                    await GoOnWithNavigation();
                }
                else
                {
                    ReleaseContent();
                    await DisplayAlert("Error", "Couldn't log in effectively. Check your username and password, then retry.", "OK");
                }
            }
            else
            {
                ReleaseContent();
                await DisplayAlert("Error", "Couldn't log in effectively. Check your username and password, then retry.", "OK");
            }
        }

        // Checks if the fields are empty
        private bool CheckIfFieldsAreEmpty()
        {
            if (UsernameEntry.Text == null)
            {
                DisplayAlert("Error", "The Username field is empty.", "OK");
                return false;
            }
            if (PasswordEntry.Text == null)
            {
                DisplayAlert("Error", "The Password field is empty.", "OK");
                return false;
            }
            return true;
        }

        // Creates the feed page
        private async Task GoOnWithNavigation()
        {
            if (Navigation.NavigationStack.Count > 1)
            {
                var existingPages = Navigation.NavigationStack.ToList();
                foreach (var page in existingPages)
                {
                    if (!page.IsVisible)
                    Navigation.RemovePage(page);
                }
            }

            // This is the easiest way to make the loginpage not turning back when back button is pressed
            Navigation.InsertPageBefore(new MainPage(), this);
            await Navigation.PopAsync().ConfigureAwait(false);
        }

        private async void LockAndDisplayWaitAnimation()
        {
            TransparentBackground.IsVisible = true;
            LoadingAnimationView.IsVisible = true;
            await TransparentBackground.FadeTo(1, 400);
            await LoadingAnimationView.FadeTo(1, 400);
        }

        private void ReleaseContent()
        {
            TransparentBackground.IsVisible = false;
            LoadingAnimationView.IsVisible = false;
        }
    }
}