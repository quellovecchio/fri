﻿using Frisoc.Classes.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Frisoc.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class HTMLPage : ContentPage
    {
        public HTMLPage(String v)
        {
            InitializeComponent();
            string htmlText = v.Replace(@"\", string.Empty);
            var html = new HtmlWebViewSource
            {
                Html = htmlText
            };
            Browser.Source = html;
        }
    }
}