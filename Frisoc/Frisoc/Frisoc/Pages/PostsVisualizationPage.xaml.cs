﻿using Frisoc.Classes;
using Frisoc.Classes.Geolocalization;
using Frisoc.Customs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Frisoc.Pages
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class PostsVisualizationPage : ContentPage
	{
        public PostsVisualizationPage()
        {
            InitializeComponent();
        }

        public PostsVisualizationPage (Post p)
		{
			InitializeComponent ();
            List<Post> posts = new List<Post>();
            posts.Add(p);
            PostLoader(posts);
		}

        public PostsVisualizationPage(List<Post> posts, Location l)
        {
            InitializeComponent();
            PageStackLayout.RotateTo(180);
            PostLoader(OrderByDistance(posts, l));
        }

        protected async void PostLoader(List<Post> posts)
        {
            foreach (Post p in posts)
                await LoadPost(p);
            await AnimatePosts();
        }


        // Copy paste from FeedPage. I know it isn't an elegant solution but here we are
        public Task LoadPost(Post p)
        {
            return Task.Run(() =>
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    PostView pe = new PostView(p);
                    PageStackLayout.Children.Add(pe);
                });
            });
        }


        // Copy paste from FeedPage. I know it isn't an elegant solution but here we are
        private Task AnimatePosts()
        {
            return Task.Run(() =>
            {
                foreach (PostView pe in PageStackLayout.Children.ToList())
                    pe.FadeTo(1, 150);
            });
        }

        private List<Post> OrderByDistance(List<Post> posts, Location l)
        {
            Dictionary<Post, double> postsDistances = GetDistances(posts, l);
            List<KeyValuePair<Post, double>> myList = postsDistances.ToList();
            myList.Sort(
                delegate (KeyValuePair<Post, double> pair1,
                KeyValuePair<Post, double> pair2)
                {
                    return (pair1.Value.CompareTo(pair2.Value));
                }
            );
            return posts;
        }

        private Dictionary<Post, double> GetDistances(List<Post> posts, Location l)
        {
            Dictionary<Post, double> distances = new Dictionary<Post, double>();
            foreach (Post p in posts)
            {
                double distance = Location.CalculateDistance(l, p.Place, 0);
                distances.Add(p, distance);
            }
            return distances;
        }
    }
}