﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using Frisoc.Pages;
using Frisoc.Classes;
using Xamarin.Forms.PlatformConfiguration.AndroidSpecific;

namespace Frisoc.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MainPage : Xamarin.Forms.TabbedPage
    {

        private UserPage userPage;

        public MainPage ()
        {
            UserPage u = new UserPage((User)Xamarin.Forms.Application.Current.Properties["user"]);
            userPage = u;
            On<Xamarin.Forms.PlatformConfiguration.Android>().SetToolbarPlacement(ToolbarPlacement.Bottom);
            this.Children.Add(new FeedPage());
            this.Children.Add(u);
            this.Children.Add(new NewPostPage());
            InitializeComponent();
        }
    }
}