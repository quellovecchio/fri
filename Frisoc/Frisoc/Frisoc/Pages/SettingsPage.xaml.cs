﻿using Frisoc.Classes;
using Frisoc.Classes.AWS;
using Frisoc.Classes.Constants;
using Frisoc.Classes.Media;
using Plugin.LocalNotifications;
using Plugin.Media;
using Plugin.Media.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Frisoc.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SettingsPage : ContentPage
    {

        private UserPage userPage;

        public SettingsPage(UserPage userPage)
        {
            InitializeComponent();
            this.userPage = userPage;
            InitializeGestureRecognizers();
        }

        private void InitializeGestureRecognizers()
        {
            var changeProfilePictureGestureRecognizer = new TapGestureRecognizer();
            changeProfilePictureGestureRecognizer.Tapped += (s, e) =>
            {
                ChangeProfilePicButton_Clicked(s, e);
            };
            var logoutGestureRecognizer = new TapGestureRecognizer();
            logoutGestureRecognizer.Tapped += (s, e) =>
            {
                LogoutButton_Clicked(s, e);
            };
            var showConditions = new TapGestureRecognizer();
            showConditions.Tapped += (s, e) =>
            {
                ShowUsageConditions();
            };
            var showPrivacyPolicy = new TapGestureRecognizer();
            showPrivacyPolicy.Tapped += (s, e) =>
            {
                ShowPrivacyPolicy();
            };
            ProfilePictureButton.GestureRecognizers.Add(changeProfilePictureGestureRecognizer);
            LogoutButton.GestureRecognizers.Add(logoutGestureRecognizer);
            PrivacyButton.GestureRecognizers.Add(showPrivacyPolicy);
            ConditionsButton.GestureRecognizers.Add(showConditions);
            NotificationsSwitch.Toggled += NotificationSwitch_Toggled;
        }

        private void ShowPrivacyPolicy()
        {
            Navigation.PushAsync(new HTMLPage(Strings.HtmlFormattedPrivacyPolicy));
        }

        private void ShowUsageConditions()
        {
            Navigation.PushAsync(new HTMLPage(Strings.HtmlFormattedUsageConditions));
        }

        private void NotificationSwitch_Toggled(object sender, ToggledEventArgs e)
        {
            Application.Current.Properties.TryAdd("areNotificationsActivated", e.Value);
        }

        private async void ChangeProfilePicButton_Clicked(object sender, EventArgs e)
        {
            bool successionFlag = false;

            var action = await DisplayActionSheet("Where to pick the photo?", "Cancel", null, "Camera", "Gallery");

            string filePath;

            if (action == "Camera")
                filePath = await MediaController.TakeAPhotoAsync(512);
            else
                filePath = await MediaController.ChoosePhotoFromGalleryAsync();

            // Create the notification indicating that the file is being sent to the server
            if((bool)Application.Current.Properties["areNotificationsActivated"])
                CrossLocalNotifications.Current.Show("Fri", "Your photo is being sent", 101);

            // Follow MediaController documentation for further information on those values
            if (filePath == "0")
            {
                // Operation aborted
                CrossLocalNotifications.Current.Cancel(101);
            }
            else if (filePath == "1")
            {
                // Camera not avaliable
                await DisplayAlert("Error", "No camera available.", "OK");
                CrossLocalNotifications.Current.Cancel(101);
            }
            else if (filePath == "2")
            {
                // Gallery pick not avaliable
                await DisplayAlert("Error", "No gallery pick available.", "OK");
                CrossLocalNotifications.Current.Cancel(101);
            }
            else
                successionFlag = true;

            if(successionFlag)
            {
                try
                {
                    if (await AWSS3Service.PutProfilePictureAsync((String)Application.Current.Properties["username"], filePath))
                    {
                        if ((bool)Application.Current.Properties["areNotificationsActivated"])
                            CrossLocalNotifications.Current.Cancel(101);

                        User u = (User)Xamarin.Forms.Application.Current.Properties["user"];
                        u.ProfilePicture.Source = ImageSource.FromFile(filePath);
                        Xamarin.Forms.Application.Current.Properties["user"] = u;
                        userPage.User = u;
                        userPage.reloadUserProfilePicture();

                        await DisplayAlert("Success", "The new photo has been set", "OK");
                    }

                    else
                    {
                        if ((bool)Application.Current.Properties["areNotificationsActivated"])
                            CrossLocalNotifications.Current.Cancel(101);

                        await DisplayAlert("Error", "The new photo has not been set. Retry", "OK");
                    }
                }
                catch (Exception ex)
                {
                    //await DisplayAlert("Error", ex.InnerException.Message, "OK");
                    CrossLocalNotifications.Current.Cancel(101);
                    await DisplayAlert("Error", "There has been a problem with the upload of the photo. Please retry", "OK");
                }
            } 
        }

        private void LogoutButton_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new LoginPage());
        }
    }
}
 