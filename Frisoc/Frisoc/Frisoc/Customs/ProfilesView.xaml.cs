﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Frisoc.Classes;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Frisoc.Customs
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ProfilesView : ContentView
    {
        private List<User> usersToVisualize;

        public ProfilesView(List<User> users)
        {
            this.usersToVisualize = users;
            InitializeComponent();
            foreach(User u in usersToVisualize)
            {
                ClickableImageCircle userImage = new ClickableImageCircle { VerticalOptions = LayoutOptions.FillAndExpand };
                userImage.User = u;
                userImage.Source = u.ProfilePicture.Source;
                UsersView.Children.Add(userImage);
            }
        }
    }
}