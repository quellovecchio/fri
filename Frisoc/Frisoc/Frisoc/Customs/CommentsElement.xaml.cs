﻿using Frisoc.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Frisoc.Customs
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class CommentsElement : ContentView
	{
		public CommentsElement (User activeUser, Dictionary<User, string> comments)
		{
			InitializeComponent ();
            this.ActiveUserCommentsProfilePic.Source = activeUser.ProfilePicture.Source;
            AddCommentsToLayout(comments);
		}

        // Needs to have propic of the commenter 
        private void AddCommentsToLayout(Dictionary<User, string> comments)
        {
            if (comments.Count == 0)
            {
                this.CommentsLayout.Children.Add(new Label() { Text = "No comments yet..." });
            }
            else
            {
                StackLayout comment = new StackLayout() { Orientation = StackOrientation.Horizontal };
                //comment.Children.Add(new Image() { Source = "Albert_Einstein.jpg" });
                comment.Children.Add(new Label() { Text = comments.First().Value });
                this.CommentsLayout.Children.Add(comment);
                if (comments.Count > 1)
                    this.CommentsLayout.Children.Add(new Label() { TextColor = Color.Gray, Text = "Show More..." });
            }
        }
    }
}