﻿using Frisoc.Classes;
using Frisoc.Classes.Geolocalization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Frisoc.Customs
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class VisitationView : ContentView
    {

        public VisitationView(Visitation v)
        {
            Rotate();
            InitializeComponent();
            UserImage.Source = v.Writer.ProfilePicture.Source;
            UserImage.User = v.Writer;
            SetDescriptionAsync(v);
        }

        private void Rotate()
        {
            this.RotateTo(180, 1);
        }

        public async Task SetDescriptionAsync(Visitation v)
        {
            FormattedString r = new FormattedString();
            r.Spans.Add(new Span { Text = v.Writer.Username + " ", FontAttributes = FontAttributes.Bold, FontSize = 16 });
            r.Spans.Add(new Span { Text = "has been in ", FontSize = 16 });
            r.Spans.Add(new Span { Text = await GeolocalizationController.GetCountryFromLocationAsync(v.Place), FontAttributes = FontAttributes.Bold, FontSize = 16 });
            Description.FormattedText = r;
            DateLabel.Text = v.PublicationDate.ToShortDateString();
        }

    }
}