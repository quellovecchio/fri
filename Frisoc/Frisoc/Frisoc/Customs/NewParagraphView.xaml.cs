﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Frisoc.Customs
{
	[XamlCompilation(XamlCompilationOptions.Compile)]

	public partial class NewParagraphView : ContentView
	{
        public static string PARAGRAPH = "° paragraph";

        public string ParagraphField_
        {
            get { return ParagraphField.Text; }
            set { ParagraphField.Text = value; }
        }

        public string ParagraphNumber_
        {
            get { return ParagraphNumber.Text; }
            set { ParagraphNumber.Text = value; }
        }

        public NewParagraphView (int n)
		{
            InitializeComponent();
            string text = String.Concat(n.ToString(), PARAGRAPH);
            ParagraphNumber.Text = text;
		}

        private void BoldButton_Clicked(object sender, EventArgs e)
        {

        }

        private void ItalicButton_Clicked(object sender, EventArgs e)
        {

        }

        private void UnderlinedButton_Clicked(object sender, EventArgs e)
        {

        }

        private void AlignLeftButton_Clicked(object sender, EventArgs e)
        {

        }

        private void AlignRightButton_Clicked(object sender, EventArgs e)
        {

        }

        private void AlignCenterButton_Clicked(object sender, EventArgs e)
        {

        }

        private void PictureButton_Clicked(object sender, EventArgs e)
        {

        }
    }
}