﻿using Frisoc.Classes;
using System;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Frisoc.Customs
{
    public class LabelClickable : Label
	{
        private String _status;
        public String status
        {
            set
            {
                if (value != _status)
                {
                    _status = value;
                    OnPropertyChanged("status");
                }
            }
            get
            {
                return _status;
            }
        }

        public LabelClickable()
		{
            TapGestureRecognizer singleTap = new TapGestureRecognizer()
            {
                NumberOfTapsRequired = 1
            };
            this.GestureRecognizers.Add(singleTap);
            singleTap.Tapped += LabelClickable_Clicked;
            // To be signup when finished
            status = "Sign In";
            Text = status;
        }

        private void LabelClickable_Clicked(object sender, EventArgs e)
        {
            if (status == "Sign In")
                status = "Sign Up";
            if (status == "Sign Up")
                status = "Sign In";
            Text = status;
        }
    }
}
