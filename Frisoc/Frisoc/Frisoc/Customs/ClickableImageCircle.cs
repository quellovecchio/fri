﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
using ImageCircle.Forms.Plugin.Abstractions;
using Frisoc.Pages;
using Frisoc.Classes;

namespace Frisoc.Customs
{
    public class ClickableImageCircle : CircleImage
    {
        private User user;
        public User User { get => user; set => user = value; }

        public ClickableImageCircle()
        {
            TapGestureRecognizer singleTap = new TapGestureRecognizer()
            {
                NumberOfTapsRequired = 1
            };
            this.GestureRecognizers.Add(singleTap);
            singleTap.Tapped += ClickableImageCircle_Clicked;
        }

        private async void ClickableImageCircle_Clicked(object sender, EventArgs e)
        {
            try
            {
                await Navigation.PushAsync(new UserPage(User));

            }
            catch (ArgumentNullException)
            {
                Console.WriteLine("Non è stato inserito l'utente nella clickable image circle");
                throw;
            }
        }
    }
}
