﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Frisoc.Classes;
using Frisoc.Pages;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Xamarin.Essentials;
using Frisoc.Classes.DB;
using Frisoc.Classes.Geolocalization;

namespace Frisoc.Customs
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class SearchView : ContentView
	{

        private FeedPage displayPage;

        public FeedPage DisplayPage { get => displayPage; set => displayPage = value; }


        public SearchView (FeedPage page)
		{
			InitializeComponent();
            DisplayPage = page;
		}


        private void FindNearButton_Clicked(object sender, EventArgs e)
        {
            DisplayPage.LockAndDisplayWaitAnimation();
            ResolveLocationAndGoOn();
        }

        private async void ResolveLocationAndGoOn()
        {
            // To rewrite utilizing GeolocalizationController
            try
            {
                var request = new GeolocationRequest(GeolocationAccuracy.Medium);
                var location = await Geolocation.GetLocationAsync(request);
                List<Post> posts = FindPostsFromPosition(location);
                PostsVisualizationPage result = new PostsVisualizationPage(posts, location);
                result.HeaderLayout.Children.Add(new SearchResultView());

                //Console.WriteLine($"Latitude: {location.Latitude}, Longitude: {location.Longitude}, Altitude: {location.Altitude}");
                await Navigation.PushAsync(result);
            }
            catch (FeatureNotSupportedException fnsEx)
            {
                await DisplayPage.DisplayAlert("Error", "It seems your device doesn't support geolocalization, so this feature cannot be accessed.", "Ok");
                DisplayPage.ReleaseContent();
            }
            catch (FeatureNotEnabledException fneEx)
            {
                await DisplayPage.DisplayAlert("Error", "It seems geolocalization is off. Turn it on to make this function work.", "Ok");
                DisplayPage.ReleaseContent();
            }
            catch (PermissionException pEx)
            {
                await DisplayPage.DisplayAlert("Error", "It seems the developer didn't give you permission for this.", "Ok");
                DisplayPage.ReleaseContent();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                await DisplayPage.DisplayAlert("Error", "It seems we could not retrieve your position. Is your geolocalization on?", "Ok");
                DisplayPage.ReleaseContent();
            }
        }

        private List<Post> FindPostsFromPosition(Location location)
        {
            DBController dbController = new DBController();
            return dbController.FindPostsInTheNearings(location);
        }

        private async void SearchButton_ClickedAsync(object sender, EventArgs e)
        {
            DisplayPage.LockAndDisplayWaitAnimation();
            DBController dbController = new DBController();
            Location l = await GeolocalizationController.GetLocationFromAddressAsync(SearchBar.Text);
            List<Post> posts = dbController.FindPostsInTheNearings(l);
            List<User> users = dbController.FindUsersByUsernameAndVisitations(SearchBar.Text, l);
            PostsVisualizationPage result = new PostsVisualizationPage(posts, l);
            result.HeaderLayout.Children.Add(new SearchResultView());
            result.HeaderLayout.Children.Add(new ProfilesView(users));
            await Navigation.PushAsync(result);
        }
    }
}