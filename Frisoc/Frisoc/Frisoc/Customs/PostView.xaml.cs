﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using Frisoc.Classes;
using static Frisoc.Classes.Formatting;
using static Frisoc.Classes.Alignament;
using Frisoc.Classes.Geolocalization;
using Xamarin.Essentials;
using System.IO;
using System.Reflection;

namespace Frisoc.Customs
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PostView : ContentView
    {
        private Post post;

        public PostView(Post post)
        {
            InitializeComponent();
            this.post = post;
            Init(post);
        }

        private void Init(Post post)
        {
            Rotate();
            BodyLabel.Text = post.TextBody;
            SetPlaceLabel(post.Place);
            ImageView.Source = post.Photo.Source;
            DateLabel.Text = post.PublicationDate.ToShortDateString();
            WriterProPic.User = post.Writer;
            WriterProPic.Source = post.Writer.ProfilePicture.Source;
            var gestureRecognizer = new TapGestureRecognizer();
            gestureRecognizer.Tapped += (s, e) =>
            {
                Navigate();
            };
            PostFrame.GestureRecognizers.Add(gestureRecognizer);
        }

        private void Rotate()
        {
            this.RotateTo(180, 1);
        }

        private async void SetPlaceLabel(Location place)
        {
            PlaceLabel.Text = await GeolocalizationController.GetAddressFromLocationAsync(place);
        }

        private async void Navigate()
        {
            var options = new MapLaunchOptions { NavigationMode = NavigationMode.Walking };
            await Map.OpenAsync(post.Place, options);
        }
    }
}