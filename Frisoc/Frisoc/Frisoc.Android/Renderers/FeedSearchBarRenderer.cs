﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xamarin.Forms.Platform.Android;
using Android.Content;
using Xamarin.Forms;
using Frisoc.Customs;
using Frisoc.Droid.Renderers;
using Android.Widget;
using Android.Graphics;

[assembly: ExportRenderer(typeof(FeedSearchBar), typeof(FeedSearchBarRenderer))]

namespace Frisoc.Droid.Renderers
{
    class FeedSearchBarRenderer : SearchBarRenderer
    {
        public FeedSearchBarRenderer(Context context) : base(context)
        {
        }

        protected override void OnElementChanged(ElementChangedEventArgs<SearchBar> e)
        {
            base.OnElementChanged(e);
            var color = global::Xamarin.Forms.Color.LightGray;
            var searchView = (Control as Android.Widget.SearchView);
            var searchIconId = searchView.Resources.GetIdentifier("android:id/search_mag_icon", null, null);
            if (searchIconId > 0)
            {
                var searchPlateIcon = searchView.FindViewById(searchIconId);
                (searchPlateIcon as ImageView).SetColorFilter(Android.Graphics.Color.White, PorterDuff.Mode.SrcIn);
            }
            var symbolView = (Control as Android.Widget.SearchView);
            var symbolIconId = symbolView.Resources.GetIdentifier("android:id/search_close_btn", null, null);
            if (symbolIconId > 0)
            {
                var symbolPlateIcon = symbolView.FindViewById(symbolIconId);
                (symbolPlateIcon as ImageView).SetColorFilter(Android.Graphics.Color.White, PorterDuff.Mode.SrcIn);
            }
            if (e.OldElement == null)
            {
                LinearLayout linearLayout = this.Control.GetChildAt(0) as LinearLayout;
                linearLayout = linearLayout.GetChildAt(2) as LinearLayout;
                linearLayout = linearLayout.GetChildAt(1) as LinearLayout;

                linearLayout.Background = null; //removes underline

                AutoCompleteTextView textView = linearLayout.GetChildAt(0) as AutoCompleteTextView; //modify for text appearance customization 
            }
            /*if (Control != null)
            {
                Control.SetBackgroundColor(global::Android.Graphics.Color.Transparent);
            }*/
        }
    }
}